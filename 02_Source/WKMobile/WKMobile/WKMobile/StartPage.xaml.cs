﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WKMobile
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class StartPage : ContentPage
	{
        public string ApiKey { get; set; }
        public StartPage ()
		{
			InitializeComponent();
		}

        async void Submit_Clicked(object sender, EventArgs e)
        {

            await Navigation.PushAsync(new MainPage());
        }
    }
}