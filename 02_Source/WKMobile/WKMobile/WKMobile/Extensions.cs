﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WKMobile
{
    static class Extensions
    {
        // Extension method for Enum
        public static bool ToBoolean(this ShowMode value)
        {
            if (value == ShowMode.Character)
                return false;
            else
                return true;
        }
    }
}
