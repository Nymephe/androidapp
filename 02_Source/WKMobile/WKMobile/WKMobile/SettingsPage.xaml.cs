﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WKMobile
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class SettingsPage : ContentPage
	{
        bool showReadingWith, showFirst;
        private App _app;

		public SettingsPage ()
		{
			InitializeComponent ();
            showReadingWith = ShowMode.Character.ToBoolean();
            showFirst = ShowMode.Character.ToBoolean();

            BindingContext = Application.Current;
            _app = Application.Current as App;

        }

        private void SwapRadioButtons(ref bool a_radioButton,
                                      ref Image a_firstRadioImg, ref Image a_secondRadioImg)
        {
            if (a_radioButton == ShowMode.Meaning.ToBoolean())
            {
                a_firstRadioImg.Source = "@drawable/uncheckedRadioPng";
                a_secondRadioImg.Source = "@drawable/checkedRadioPng";
            }
            else if (a_radioButton == ShowMode.Character.ToBoolean())
            {
                a_firstRadioImg.Source = "@drawable/checkedRadioPng";
                a_secondRadioImg.Source = "@drawable/uncheckedRadioPng";
            }
        }

        private void ShowReadingWithCharacter_Tapped(object sender, EventArgs e)
        {
            showReadingWith = ShowMode.Character.ToBoolean();
            if (showReadingWith != _app.ReadingWith)
            {
                _app.ReadingWith = ShowMode.Character.ToBoolean();
                SwapRadioButtons(ref showReadingWith, ref showReadingWithCharacter, ref showReadingWithMeaning);
            }
        }

        private void ShowReadingWithMeaning_Tapped(object sender, EventArgs e)
        {
            showReadingWith = ShowMode.Meaning.ToBoolean();
            if (showReadingWith != _app.ReadingWith)
            {
                _app.ReadingWith = ShowMode.Meaning.ToBoolean();
                SwapRadioButtons(ref showReadingWith, ref showReadingWithCharacter, ref showReadingWithMeaning);
            }
        }

        private void ShowFirstCharacter_Tapped(object sender, EventArgs e)
        {
            showFirst = ShowMode.Character.ToBoolean();
            if (showFirst != _app.ShowWith)
            {
                _app.ShowWith = ShowMode.Character.ToBoolean();
                SwapRadioButtons(ref showFirst, ref showFirstCharacterCheck, ref showFirstMeaningCheck);
            }
        }

        private void ShowFirstMeaning_Tapped(object sender, EventArgs e)
        {
            showFirst = ShowMode.Meaning.ToBoolean();
            if (showFirst != _app.ShowWith)
            {
                _app.ShowWith = ShowMode.Meaning.ToBoolean();
                SwapRadioButtons(ref showFirst, ref showFirstCharacterCheck, ref showFirstMeaningCheck);
            }
        }

        async private void RemoveShameListButton_Clicked(object sender, EventArgs e)
        {
            var response = await DisplayAlert("","Are you sure you want to remove shame list from this device?", "Yes", "No");
            if (response)
            {
                await DisplayAlert("Done","Shame list has been removed","OK");
            }
        }

        async private void SetNotification_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NotificationPage());
        }

        //protected override void OnDisappearing()
        //{
        //    base.OnDisappearing(); 
        //}
    }
}