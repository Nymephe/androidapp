﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WKMobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WKMobile
{
    [XamlCompilation(XamlCompilationOptions.Skip)]
    public partial class MainMenuMasterPage : ContentPage
    {
        public ListView ListView { get { return listView; } }

        //ListView listView;

        public List<MainMenuItem> MainMenuItems { get; set; }
        public MainMenuMasterPage()
        {
            InitializeComponent();
            // Build the Menu

            listView.ItemsSource = new List<MainMenuItem>()
            {
                new MainMenuItem() { Title = "Home", TargetType = typeof(MainPage)},
                new MainMenuItem() { Title = "Levels", TargetType = typeof(MainPage)},
                new MainMenuItem() { Title = "Radicals", TargetType = typeof(MainPage)},
                new MainMenuItem() { Title = "Kanji", TargetType = typeof(MainPage)},
                new MainMenuItem() { Title = "Vocab", TargetType = typeof(MainPage)},
                new MainMenuItem() { Title = "Shame list", TargetType = typeof(MainPage)},
                new MainMenuItem() { Title = "Settings", TargetType = typeof(SettingsPage)}
            };

        }
    }
}