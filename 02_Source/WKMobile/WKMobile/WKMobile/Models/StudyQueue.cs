﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WKMobile.Models
{
    public class StudyQueue
    {
        [JsonProperty("lessons_available")]
        public long LessonsAvailable { get; set; }

        [JsonProperty("reviews_available")]
        public long ReviewsAvailable { get; set; }

        [JsonProperty("next_review_date")]
        public long NextReviewDate { get; set; }

        [JsonProperty("reviews_available_next_hour")]
        public long ReviewsAvailableNextHour { get; set; }

        [JsonProperty("reviews_available_next_day")]
        public long ReviewsAvailableNextDay { get; set; }
    }
}
