﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WKMobile.Models
{
    public class MainMenuItem
    {
        public string Title { get; set; }
        public Type TargetType { get; set; }
    }
}
