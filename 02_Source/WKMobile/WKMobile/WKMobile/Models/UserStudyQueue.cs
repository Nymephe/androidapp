﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WKMobile.Models
{
    public class UserStudyQueue
    {
        [JsonProperty("user_information")]
        public UserInformation UserInformation { get; set; }

        [JsonProperty("requested_information")]
        public StudyQueue RequestedInformation { get; set; }
    }
}
