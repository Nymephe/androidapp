﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WKMobile.Models
{
    class UserSpecific
    {
        [JsonProperty("srs")]
        public string Srs { get; set; }

        [JsonProperty("srs_numeric")]
        public long SrsNumeric { get; set; }

        [JsonProperty("unlocked_date")]
        public long UnlockedDate { get; set; }

        [JsonProperty("available_date")]
        public long AvailableDate { get; set; }

        [JsonProperty("burned")]
        public bool Burned { get; set; }

        [JsonProperty("burned_date")]
        public long BurnedDate { get; set; }

        [JsonProperty("meaning_correct")]
        public int? MeaningCorrect { get; set; }

        [JsonProperty("meaning_incorrect")]
        public int? MeaningIncorrect { get; set; }

        [JsonProperty("meaning_max_streak")]
        public int? MeaningMaxStreak { get; set; }

        [JsonProperty("meaning_current_streak")]
        public int? MeaningCurrentStreak { get; set; }

        [JsonProperty("reading_correct")]
        public int? ReadingCorrect { get; set; }

        [JsonProperty("reading_incorrect")]
        public int? ReadingIncorrect { get; set; }

        [JsonProperty("reading_max_streak")]
        public int? ReadingMaxStreak { get; set; }

        [JsonProperty("reading_current_streak")]
        public int? ReadingCurrentStreak { get; set; }

        [JsonProperty("meaning_note")]
        public string MeaningNote { get; set; }

        [JsonProperty("reading_note")]
        public string ReadingNote { get; set; }

        [JsonProperty("user_synonyms")]
        public string UserSynonyms { get; set; }
    }
}
