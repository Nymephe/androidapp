﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WKMobile.Models
{
    public partial class UserInformation
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("gravatar")]
        public string Gravatar { get; set; }

        [JsonProperty("level")]
        public long Level { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("about")]
        public string About { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        [JsonProperty("topics_count")]
        public long TopicsCount { get; set; }

        [JsonProperty("posts_count")]
        public long PostsCount { get; set; }

        [JsonProperty("creation_date")]
        public long CreationDate { get; set; }

        [JsonProperty("vacation_date")]
        public int? VacationDate { get; set; }
    }
}
