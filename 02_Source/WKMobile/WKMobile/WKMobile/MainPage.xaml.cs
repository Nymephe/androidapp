﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WKMobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WKMobile
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class MainPage : MasterDetailPage
    {
        public MainPage ()
		{
            InitializeComponent();
            masterPage.listView.ItemSelected += OnItemSelected;
        }

        // When a MenuItem is selected.
        public void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MainMenuItem;
            if (item != null)
            {
                Detail = (Page)Activator.CreateInstance(item.TargetType);
                masterPage.listView.SelectedItem = null;
                IsPresented = false;
            }
        }
    }
}