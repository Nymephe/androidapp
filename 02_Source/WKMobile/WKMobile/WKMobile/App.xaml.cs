﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace WKMobile
{
    public enum ShowMode { Character = 0, Meaning = 1 };

    public partial class App : Application
    {
        private const string NightModeKey = "NightMode";
        private const string ReadingWithKey = "ReadingWith";
        private const string ShowWithKey = "ShowWith";
        private const string NotificationKey = "Notification";
        private const string NotificationTimeKey = "NotificationTime";
        TimeSpan _defaultNotificationTime;


        public App()
        {
            InitializeComponent();

            MainPage = new WKMobile.MainPage();
            _defaultNotificationTime = new TimeSpan(12,00,00);
                                            
        }
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public bool NightMode
        {
            get
            {
                if (Properties.ContainsKey(NightModeKey))
                    return (bool) Properties[NightModeKey];
                else
                    return ShowMode.Character.ToBoolean();
            }
            set
            {
                Properties[NightModeKey] = value;
            }
        }

        public bool ReadingWith
        {
            get
            {
                if (Properties.ContainsKey(ReadingWithKey))
                    return (bool)Properties[ReadingWithKey];
                else
                    return ShowMode.Character.ToBoolean();
            }
            set
            {
                Properties[ReadingWithKey] = value;
            }
        }

        public bool ShowWith
        {
            get
            {
                if (Properties.ContainsKey(ShowWithKey))
                    return (bool)Properties[ShowWithKey];
                else
                    return false;
            }
            set
            {
                Properties[ShowWithKey] = value;
            }
        }

        public bool Notification
        {
            get
            {
                if (Properties.ContainsKey(NotificationKey))
                {
                    return (bool)Properties[NotificationKey];
                }
                else
                {
                    return false;
                }
            }
            set
            {
                Properties[NotificationKey] = value;
            }
        }
        public TimeSpan NotificationTime
        {
            get
            {
                if (Properties.ContainsKey(NotificationTimeKey))
                {
                    return (TimeSpan)Properties[NotificationTimeKey];
                }
                else
                {
                    return _defaultNotificationTime;
                }
            }
            set
            {
                Properties[NotificationTimeKey] = value;
            }
        }
    }
}
