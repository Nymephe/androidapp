﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WKMobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WKMobile
{
	[XamlCompilation(XamlCompilationOptions.Skip)]
	public partial class UserInfoPage : ContentPage
	{
        private const string Url = "https://www.wanikani.com/api/user/955226a2a23e4d3eff8e64d9564b4b7a/study-queue";
        private HttpClient _client = new HttpClient();
        private UserStudyQueue _userQueueInfo;

        public UserInfoPage ()
		{
			InitializeComponent ();
        }

        protected override async void OnAppearing()
        {
            var content = await _client.GetStringAsync(Url);
            var userQueue = JsonConvert.DeserializeObject< UserStudyQueue>(content);
            _userQueueInfo = userQueue;

            UserNameLabel.Text = _userQueueInfo.UserInformation.Username;
            WaitingInReview.Text = _userQueueInfo.RequestedInformation.ReviewsAvailable.ToString();
            WaitingToLearn.Text = _userQueueInfo.RequestedInformation.LessonsAvailable.ToString();
            CurrentLvl.Text = _userQueueInfo.UserInformation.Level.ToString();
        }

    }
}