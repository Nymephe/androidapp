package mobile.wanikani.wkmobileandroid.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class RequestedRadicalList {

    @JsonProperty("user_information")
    private UserInformation userInformation;
    @JsonProperty("requested_information")
    private List<Radical> radicalList = null;

    @JsonProperty("user_information")
    public UserInformation getUserInformation() {
        return userInformation;
    }

    @JsonProperty("user_information")
    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    @JsonProperty("requested_information")
    public List<Radical> getRequestedInformation() {
        return radicalList;
    }

    @JsonProperty("requested_information")
    public void setRequestedInformation(List<Radical> requestedInformation) {
        this.radicalList = requestedInformation;
    }

}