package mobile.wanikani.wkmobileandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import mobile.wanikani.wkmobileandroid.dataproviders.DownloadStatus;
import mobile.wanikani.wkmobileandroid.dataproviders.GetKanjiJsonData;
import mobile.wanikani.wkmobileandroid.dataproviders.GetRadicalJsonData;
import mobile.wanikani.wkmobileandroid.dataproviders.GetVocabJsonData;
import mobile.wanikani.wkmobileandroid.models.BasicData;
import mobile.wanikani.wkmobileandroid.models.Kanji;
import mobile.wanikani.wkmobileandroid.models.LearningMode;
import mobile.wanikani.wkmobileandroid.models.Radical;
import mobile.wanikani.wkmobileandroid.models.RequestedKanjiList;
import mobile.wanikani.wkmobileandroid.models.RequestedRadicalList;
import mobile.wanikani.wkmobileandroid.models.RequestedVocabList;
import mobile.wanikani.wkmobileandroid.models.Vocab;

import static android.content.Context.MODE_PRIVATE;

public class DetailListFragment extends Fragment implements RecyclerItemClickListener.OnRecyclerClickListener,
        GetVocabJsonData.OnDataAvailable,
        GetKanjiJsonData.OnDataAvailable,
        GetRadicalJsonData.OnDataAvailable {

    private static final String TAG = "DetailListFragment";
    LearningMode mSelectedLearningMode;
    private SharedPreferences mSettings;
    private ArrayList<Integer> mSelectedLevelsList;
    private final String mVocabRequest = "vocabulary";
    private final String mKanjiRequest = "kanji";
    private final String mRadicalRequest = "radicals";
    private RecyclerView mListRecyclerView;
    private RecyclerView.Adapter mDetailListRecyclerViewAdapter;
    ArrayList<Radical> mRadicalList;
    ArrayList<Kanji> mKanjiList;
    ArrayList<Vocab> mVocabList;

    private final List<Integer> mLvls = IntStream.range(1, 60 + 1)
            .boxed()
            .collect(Collectors.toList());


    public static Fragment getInstance(LearningMode mode, ArrayList<Integer> selectedLvls) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(LearningMode.LEARNING_MODE, mode);
        bundle.putSerializable("SelectedLevels", selectedLvls);

        DetailListFragment tabFragment = new DetailListFragment();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelectedLearningMode = (LearningMode) getArguments().getSerializable(LearningMode.LEARNING_MODE);
        mSelectedLevelsList = (ArrayList<Integer>) getArguments().getSerializable("SelectedLevels");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_detail_list, container, false);

        Button okButton = (Button) rootView.findViewById(R.id.detail_ok);
        okButton.setOnClickListener(new View.OnClickListener()
        {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent( getContext(), LearningActivity.class);
                    intent.putExtra(LearningMode.LEARNING_MODE, mSelectedLearningMode);

                    if (LearningMode.KANJI == mSelectedLearningMode) {
                        intent.putExtra("kanji_list", mKanjiList);
                    } else if (LearningMode.RADICALS == mSelectedLearningMode) {
                        intent.putExtra("radicals_list", mRadicalList);
                    } else if (LearningMode.VOCAB == mSelectedLearningMode) {
                        intent.putExtra("vocab_list", mVocabList);
                    }
                    startActivity(intent);
                }
        });

        mSettings = getContext().getSharedPreferences("AppSettings", MODE_PRIVATE);

        mListRecyclerView = rootView.findViewById(R.id.detail_list);
        mListRecyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        mListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), mListRecyclerView, this));

        if (LearningMode.VOCAB == mSelectedLearningMode) {
            Log.d(TAG, "onCreate: setting recycler to VOCAB");
            mDetailListRecyclerViewAdapter = new VocabDetailListRecyclerViewAdapter(
                    getContext(), new ArrayList<>(Arrays.asList(Vocab.getDummyVocab())));

            if (null == mSelectedLevelsList) {
                SharedPreferences shamelist  = getContext().getSharedPreferences(Vocab.SHAME_LIST, MODE_PRIVATE);
                ObjectMapper objectMapper = new ObjectMapper();
                List<Vocab> vocabList = new ArrayList<>();
                Map<String, ?> allEntries = shamelist.getAll();

                try {
                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                        vocabList.add(objectMapper.readValue(entry.getValue().toString(), Vocab.class));
                    }
                    saveFragmentVocabList(vocabList);
                } catch (JsonParseException e ) {
                    Log.d(TAG, "onCreateView: JsonParseException while parsing shamelist");
                } catch (JsonMappingException e ) {
                    Log.d(TAG, "onCreateView: JsonMappingException while parsing shamelist");
                } catch (IOException e) {
                    Log.d(TAG, "onCreateView: IOException while parsing shamelist");
                }
                VocabDetailListRecyclerViewAdapter adapter = (VocabDetailListRecyclerViewAdapter) mDetailListRecyclerViewAdapter;
                adapter.loadNewData(vocabList);

                saveFragmentVocabList(vocabList);
            } else {
                GetVocabJsonData getJsonData = new GetVocabJsonData(this);
                getJsonData.execute(mSettings.getString(getString(R.string.login_api_key), null), createRequest(mVocabRequest));
            }

        } else if (LearningMode.KANJI == mSelectedLearningMode) {
            Log.d(TAG, "onCreate: setting recycler to KANJI");
            mDetailListRecyclerViewAdapter = new KanjiDetailListRecyclerViewAdapter(
                    getContext(), new ArrayList<>(Arrays.asList(Kanji.getDummyKanji())));

            if (null == mSelectedLevelsList) {
                SharedPreferences shamelist  = getContext().getSharedPreferences(Kanji.SHAME_LIST, MODE_PRIVATE);
                ObjectMapper objectMapper = new ObjectMapper();
                List<Kanji> kanjiList = new ArrayList<>();
                Map<String, ?> allEntries = shamelist.getAll();

                try {
                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                        kanjiList.add(objectMapper.readValue(entry.getValue().toString(), Kanji.class));
                    }
                    saveFragmentKanjiList(kanjiList);
                } catch (JsonParseException e ) {
                    Log.d(TAG, "onCreateView: JsonParseException while parsing shamelist");
                } catch (JsonMappingException e ) {
                    Log.d(TAG, "onCreateView: JsonMappingException while parsing shamelist");
                } catch (IOException e) {
                    Log.d(TAG, "onCreateView: IOException while parsing shamelist");
                }
                KanjiDetailListRecyclerViewAdapter adapter = (KanjiDetailListRecyclerViewAdapter) mDetailListRecyclerViewAdapter;
                adapter.loadNewData(kanjiList);

                saveFragmentKanjiList(kanjiList);
            } else {
                GetKanjiJsonData getJsonData = new GetKanjiJsonData(this);
                getJsonData.execute(mSettings.getString(getString(R.string.login_api_key), null), createRequest(mKanjiRequest));
            }
        } else if (LearningMode.RADICALS == mSelectedLearningMode) {
            Log.d(TAG, "onCreate: setting recycler to RADICALS");
            mDetailListRecyclerViewAdapter = new RadicalDetailListRecyclerViewAdapter(
                    getContext(), new ArrayList<>(Arrays.asList(Radical.getDummyRadical())));

            if (null == mSelectedLevelsList) {
                SharedPreferences shamelist  = getContext().getSharedPreferences(Radical.SHAME_LIST, MODE_PRIVATE);
                ObjectMapper objectMapper = new ObjectMapper();
                List<Radical> radicalList = new ArrayList<>();
                Map<String, ?> allEntries = shamelist.getAll();

                try {
                    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                        radicalList.add(objectMapper.readValue(entry.getValue().toString(), Radical.class));
                    }
                    RadicalDetailListRecyclerViewAdapter adapter = (RadicalDetailListRecyclerViewAdapter) mDetailListRecyclerViewAdapter;
                    adapter.loadNewData(radicalList);

                    saveFragmentRadicalList(radicalList);
                } catch (JsonParseException e ) {
                    Log.d(TAG, "onCreateView: JsonParseException while parsing shamelist");
                } catch (JsonMappingException e ) {
                    Log.d(TAG, "onCreateView: JsonMappingException while parsing shamelist");
                } catch (IOException e) {
                    Log.d(TAG, "onCreateView: IOException while parsing shamelist");
                }

            } else {
                GetRadicalJsonData getJsonData = new GetRadicalJsonData(this);
                getJsonData.execute(mSettings.getString(getString(R.string.login_api_key),null), createRequest(mRadicalRequest));
            }
        }

        mListRecyclerView.setAdapter(mDetailListRecyclerViewAdapter);

        return rootView;
    }

    public void saveFragmentRadicalList(List<Radical> radicalList) {
        mRadicalList = new ArrayList<Radical>(radicalList);
    }

    public void saveFragmentVocabList(List<Vocab> vocabList) {
        mVocabList = new ArrayList<Vocab>(vocabList);
    }

    public void saveFragmentKanjiList(List<Kanji> kanjiList) {
        mKanjiList = new ArrayList<Kanji>(kanjiList);
    }

    @Override
    public void onItemClick(View view, int position) {
        boolean updatedStatus;
        ImageView itemImage = null;
        BasicData data = new BasicData();
        SharedPreferences shamelist = null;
        String idString = null;

        if (LearningMode.RADICALS == mSelectedLearningMode) {
            itemImage = view.findViewById(R.id.radical_detail_image);
            data = mRadicalList.get(position);
            idString = ((Radical)data).getIdString();
            shamelist = getContext().getSharedPreferences(Radical.SHAME_LIST, MODE_PRIVATE);
        } else if (LearningMode.KANJI == mSelectedLearningMode) {
            itemImage = view.findViewById(R.id.kanji_detail_image);
            data = mKanjiList.get(position);
            idString = ((Kanji) data).getCharacter();
            shamelist = getContext().getSharedPreferences(Kanji.SHAME_LIST, MODE_PRIVATE);
        } else if (LearningMode.VOCAB == mSelectedLearningMode) {
            itemImage = view.findViewById(R.id.vocab_detail_image);
            data = mVocabList.get(position);
            idString = ((Vocab) data).getCharacter();
            shamelist = getContext().getSharedPreferences(Vocab.SHAME_LIST, MODE_PRIVATE);
        }

        updatedStatus = data.updateShamelistItem(shamelist, shamelist.edit(), idString, getContext());
        if (true == updatedStatus) {
            itemImage.setImageResource(R.drawable.emoji_red);
        } else {
            itemImage.setImageResource(R.drawable.emoji_gray);
        }
    }



    @Override
    public void onDataAvailable(RequestedKanjiList data, DownloadStatus status) {
        Log.d(TAG, "onDataAvailable: KANJI");
        KanjiDetailListRecyclerViewAdapter adapter = (KanjiDetailListRecyclerViewAdapter) mDetailListRecyclerViewAdapter;
        adapter.loadNewData(data.getRequestedInformation());

        saveFragmentKanjiList(data.getRequestedInformation());
    }

    @Override
    public void onDataAvailable(RequestedVocabList data, DownloadStatus status) {
        //TODO: there should be an info that downloading failed
        Log.d(TAG, "onDataAvailable: VOCAB");
        VocabDetailListRecyclerViewAdapter adapter = (VocabDetailListRecyclerViewAdapter) mDetailListRecyclerViewAdapter;
        adapter.loadNewData(data.getRequestedInformation());

        saveFragmentVocabList(data.getRequestedInformation());
    }

    @Override
    public void onDataAvailable(RequestedRadicalList data, DownloadStatus status) {
        Log.d(TAG, "onDataAvailable: RADICAL");
        RadicalDetailListRecyclerViewAdapter adapter = (RadicalDetailListRecyclerViewAdapter) mDetailListRecyclerViewAdapter;
        adapter.loadNewData(data.getRequestedInformation());

        saveFragmentRadicalList(data.getRequestedInformation());
    }

    private String createRequest(String requiredDataTypeRequest) {
        String request = requiredDataTypeRequest + "/";

        for (Integer lvl : mSelectedLevelsList) {
            request += Integer.toString(lvl) + ",";
        }

        return request;
    }
}
