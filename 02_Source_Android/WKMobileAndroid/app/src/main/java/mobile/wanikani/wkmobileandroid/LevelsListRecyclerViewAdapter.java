package mobile.wanikani.wkmobileandroid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.wanikani.wkmobileandroid.models.LevelListItem;

public class LevelsListRecyclerViewAdapter extends RecyclerView.Adapter<LevelsListRecyclerViewAdapter.ListItemViewHolder> {

    private static final String TAG = "LevelsListRecyclerViewA";

    private List<LevelListItem> mLevelItemList;
    private Context mContext;

    public LevelsListRecyclerViewAdapter(Context context, List<LevelListItem> items) {
        mContext = context;
        mLevelItemList = items;
    }

    public void updateStateList(boolean state, int pos) {
        mLevelItemList.get(pos).checked = state;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Called by the layout manager when it needs a new view
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.level_list_item, parent, false);
        return new ListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        // Called by the layout manager when it wants new data in an existing row

        holder.text.setText(mLevelItemList.get(position).text);
        holder.checkBox.setChecked(mLevelItemList.get(position).checked);
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: called");
        return ((mLevelItemList != null) && (mLevelItemList.size() !=0) ? mLevelItemList.size() : 0);
    }

    void loadNewData(List<LevelListItem> newItemList) {
        mLevelItemList = newItemList;
        notifyDataSetChanged();
    }

    ArrayList<Integer> getCheckedLevelsList() {
        ArrayList<Integer> checkedItemList = new ArrayList<>();

        for (LevelListItem item : mLevelItemList) {
            if (true == item.checked) checkedItemList.add(item.levelId);
        }
        return checkedItemList;
    }

    public LevelListItem getItem(int position) {
        return ((mLevelItemList != null) && (mLevelItemList.size() !=0) ? mLevelItemList.get(position) : null);
    }

    static class ListItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ListItemViewHolder";
        TextView text;
        CheckBox checkBox;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ListItemViewHolder: ");
            this.text = itemView.findViewById(R.id.levels_level);
            this.checkBox = itemView.findViewById(R.id.levels_checkbox);
        }
    }
 }
