package mobile.wanikani.wkmobileandroid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.wanikani.wkmobileandroid.models.Vocab;

public class VocabDetailListRecyclerViewAdapter extends RecyclerView.Adapter<VocabDetailListRecyclerViewAdapter.VocabListDetailItemViewHolder> {

    private ArrayList<Vocab> mVocabList;
    private Context mContext;

    public VocabDetailListRecyclerViewAdapter(Context context, ArrayList<Vocab> items) {
        this.mContext = context;
        this.mVocabList = items;
    }

    @NonNull
    @Override
    public VocabListDetailItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.vocab_detail_list_item, parent, false);
        return new VocabDetailListRecyclerViewAdapter.VocabListDetailItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VocabListDetailItemViewHolder holder, int position) {
        holder.idx.setText(Integer.toString(position + 1 ));
        holder.word.setText(mVocabList.get(position).getCharacter());
        holder.kana.setText(mVocabList.get(position).getKana());
        String meaning = mVocabList.get(position).getMeaning();
        if (null != mVocabList.get(position).getUserSpecific() &&
            null != mVocabList.get(position).getUserSpecific().getUserSynonyms()) {
            meaning += ", " + TextUtils.join(", ", mVocabList.get(position).getUserSpecific().getUserSynonyms());
        }
        holder.meaning.setText(meaning);

        if (mVocabList.get(position).isItemOnShamelist(mContext)) {
            holder.shameImage.setImageResource(R.drawable.emoji_red);
        } else {
            holder.shameImage.setImageResource(R.drawable.emoji_gray);
        }
    }

    @Override
    public int getItemCount() {
        return ((mVocabList != null) && (mVocabList.size() !=0) ? mVocabList.size() : 0);
    }

    public void loadNewData(List<Vocab> newItemList) {
        mVocabList = new ArrayList<Vocab>(newItemList);
        notifyDataSetChanged();
    }

    static class VocabListDetailItemViewHolder extends RecyclerView.ViewHolder {
        TextView word;
        TextView kana;
        TextView meaning;
        ImageView shameImage;
        TextView idx;

        public VocabListDetailItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.word = itemView.findViewById(R.id.vocab_detail_word);
            this.kana = itemView.findViewById(R.id.vocab_detail_kana);
            this.meaning = itemView.findViewById(R.id.vocab_detail_meaning);
            this.shameImage = itemView.findViewById(R.id.vocab_detail_image);
            this.idx = itemView.findViewById(R.id.vocab_detail_number);
        }
    }
}