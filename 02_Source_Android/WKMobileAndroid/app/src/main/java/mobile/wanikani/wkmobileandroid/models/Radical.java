package mobile.wanikani.wkmobileandroid.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import static android.content.Context.MODE_PRIVATE;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Radical extends BasicData implements Serializable {

    final public static String SHAME_LIST = "RadicalShamelist";

    @JsonProperty("level")
    private Integer level;
    @JsonProperty("character")
    private String character;
    @JsonProperty("meaning")
    private String meaning;
    @JsonProperty("image_file_name")
    private String imageFileName;
    @JsonProperty("image_content_type")
    private String imageContentType;
    @JsonProperty("image_file_size")
    private Integer imageFileSize;
    @JsonProperty("user_specific")
    private UserSpecific userSpecific;
    @JsonProperty("image")
    private String image;

    public Radical() {
    }

    @JsonProperty("level")
    public Integer getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(Integer level) {
        this.level = level;
    }

    @JsonProperty("character")
    public String getCharacter() {
        return character;
    }

    @JsonProperty("character")
    public void setCharacter(String character) {
        this.character = character;
    }

    @JsonProperty("meaning")
    public String getMeaning() {
        return meaning;
    }

    @JsonProperty("meaning")
    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    @JsonProperty("image_file_name")
    public String getImageFileName() {
        return imageFileName;
    }

    @JsonProperty("image_file_name")
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    @JsonProperty("image_content_type")
    public String getImageContentType() {
        return imageContentType;
    }

    @JsonProperty("image_content_type")
    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    @JsonProperty("image_file_size")
    public Integer getImageFileSize() {
        return imageFileSize;
    }

    @JsonProperty("image_file_size")
    public void setImageFileSize(Integer imageFileSize) {
        this.imageFileSize = imageFileSize;
    }

    @JsonProperty("user_specific")
    public UserSpecific getUserSpecific() {
        return userSpecific;
    }

    @JsonProperty("user_specific")
    public void setUserSpecific(UserSpecific userSpecific) {
        this.userSpecific = userSpecific;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    public String getIdString() {
        String idString;
        if (null != getImage() && false == getImage().equalsIgnoreCase("None") && false == getImage().isEmpty()) {
            idString = getImage();
        } else {
            idString = getCharacter();
        }
        return idString;
    }

    public boolean isItemOnShamelist(Context context) {
        SharedPreferences radicalShamelist = context.getSharedPreferences(SHAME_LIST, MODE_PRIVATE);
        SharedPreferences.Editor editor = radicalShamelist.edit();
        return (null != radicalShamelist.getString(getIdString(), null));
    }

    static public Radical getDummyRadical() {
        return new Radical() {
            {
                setCharacter("Please wait");
                setMeaning("");
            }
        };
    }
}