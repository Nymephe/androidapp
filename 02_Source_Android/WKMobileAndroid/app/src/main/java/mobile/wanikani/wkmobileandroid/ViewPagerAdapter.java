package mobile.wanikani.wkmobileandroid;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mobile.wanikani.wkmobileandroid.models.LearningMode;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "ViewPagerAdapter";
    private ArrayList<Integer> mSelectedLevels;
    Map<Integer, Map.Entry<String, LearningMode>> tabTitlesMap;// = ;

    public ViewPagerAdapter(FragmentManager manager, Context context, @Nullable ArrayList<Integer> selectedLvls) {
        super(manager);

        mSelectedLevels = selectedLvls;

        tabTitlesMap = new HashMap<Integer, Map.Entry<String, LearningMode>>() {{
            put(0, new SimpleEntry<>(context.getString(R.string.menu_radicals), LearningMode.RADICALS));
            put(1, new SimpleEntry<>(context.getString(R.string.menu_kanji), LearningMode.KANJI));
            put(2, new SimpleEntry<>(context.getString(R.string.menu_vocab), LearningMode.VOCAB));
        }};
    }

    @Override
    public Fragment getItem(int position) {
        return DetailListFragment.getInstance(tabTitlesMap.get(position).getValue(), mSelectedLevels);
    }

    @Override
    public int getCount() {
        return (null == tabTitlesMap) ? 0 : tabTitlesMap.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitlesMap.get(position).getKey();
    }
}
