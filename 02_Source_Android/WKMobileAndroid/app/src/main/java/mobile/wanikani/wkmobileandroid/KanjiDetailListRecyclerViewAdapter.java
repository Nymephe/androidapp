package mobile.wanikani.wkmobileandroid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.wanikani.wkmobileandroid.models.Kanji;

public class KanjiDetailListRecyclerViewAdapter extends RecyclerView.Adapter<KanjiDetailListRecyclerViewAdapter.KanjiListDetailItemViewHolder> {

    private ArrayList<Kanji> mKanjiList;
    private Context mContext;

    public KanjiDetailListRecyclerViewAdapter(Context context, ArrayList<Kanji> items) {
        this.mContext = context;
        this.mKanjiList = items;
    }

    @NonNull
    @Override
    public KanjiDetailListRecyclerViewAdapter.KanjiListDetailItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.kanji_detail_list_item, parent, false);
        return new KanjiDetailListRecyclerViewAdapter.KanjiListDetailItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KanjiDetailListRecyclerViewAdapter.KanjiListDetailItemViewHolder holder, int position) {
        holder.idx.setText(Integer.toString(position + 1 ));
        holder.character.setText(mKanjiList.get(position).getCharacter());

        String nanori = mKanjiList.get(position).getNanori();
        if (null != nanori && false == nanori.equalsIgnoreCase("None") && false == nanori.isEmpty()) {
            holder.nanori.setVisibility(View.VISIBLE);
            holder.nanori.setText(mContext.getString(R.string.nanori) + nanori);
        } else {
            holder.nanori.setVisibility(View.GONE);
        }

        String onyomi = mKanjiList.get(position).getOnyomi();
        if (null != onyomi && false == onyomi.equalsIgnoreCase("None") && false == onyomi.isEmpty()) {
            holder.onyomi.setVisibility(View.VISIBLE);
            holder.onyomi.setText(mContext.getString(R.string.onyomi) + onyomi);
        } else {
            holder.onyomi.setVisibility(View.GONE);
        }

        String kunyomi = mKanjiList.get(position).getKunyomi();
        if (null != kunyomi && false == kunyomi.equalsIgnoreCase("None") && false == kunyomi.isEmpty()) {

            holder.kunyomi.setVisibility(View.VISIBLE);
            holder.kunyomi.setText(mContext.getString(R.string.kunyomi) + kunyomi);
        } else {
            holder.kunyomi.setVisibility(View.GONE);
        }

        String meaning = mKanjiList.get(position).getMeaning();
        if (null != mKanjiList.get(position).getUserSpecific() &&
                null != mKanjiList.get(position).getUserSpecific().getUserSynonyms()) {
            meaning += ", " + TextUtils.join(", ", mKanjiList.get(position).getUserSpecific().getUserSynonyms());
        }
        holder.meaning.setText(meaning);

        if (mKanjiList.get(position).isItemOnShamelist(mContext)) {
            holder.shameImage.setImageResource(R.drawable.emoji_red);
        } else {
            holder.shameImage.setImageResource(R.drawable.emoji_gray);
        }
    }

    @Override
    public int getItemCount() {
        return ((mKanjiList != null) && (mKanjiList.size() !=0) ? mKanjiList.size() : 0);
    }

    public void loadNewData(List<Kanji> newItemList) {
        mKanjiList = new ArrayList<Kanji>(newItemList);
        notifyDataSetChanged();
    }

    static class KanjiListDetailItemViewHolder extends RecyclerView.ViewHolder {
        TextView character;
        TextView onyomi;
        TextView kunyomi;
        TextView nanori;
        TextView meaning;
        ImageView shameImage;
        TextView idx;

        public KanjiListDetailItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.character = itemView.findViewById(R.id.kanji_detail_character);
            this.onyomi = itemView.findViewById(R.id.kanji_detail_onyomi);
            this.kunyomi = itemView.findViewById(R.id.kanji_detail_kunyomi);
            this.nanori = itemView.findViewById(R.id.kanji_detail_nanori);
            this.meaning = itemView.findViewById(R.id.kanji_detail_meaning);
            this.shameImage = itemView.findViewById(R.id.kanji_detail_image);
            this.idx = itemView.findViewById(R.id.kanji_detail_number);
        }
    }
}
