package mobile.wanikani.wkmobileandroid.dataproviders;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import mobile.wanikani.wkmobileandroid.models.RequestedStudyQueue;

public class GetJsonData extends AsyncTask<String, Void, RequestedStudyQueue> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetJsonData";

    private RequestedStudyQueue mRequestedStudyQueue = null;
    private DownloadStatus mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;

    private final OnDataAvailable mCallBack;

    public interface OnDataAvailable {
        void onDataAvailable(RequestedStudyQueue data, DownloadStatus status);
    }

    public GetJsonData(OnDataAvailable callBack) {
        Log.d(TAG, "GetJsonData called");
        mCallBack = callBack;
    }

    protected void onPostExecute(RequestedStudyQueue studyQueue) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(mRequestedStudyQueue, mDownloadStatus);
        }
        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected RequestedStudyQueue doInBackground(String... params) {
        Log.d(TAG, "doInBackground starts");
        String destinationUri =  createUri(params[0], ((params.length > 1)? params[1] : ""));

        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground ends");
        return mRequestedStudyQueue;
    }

    private String createUri(String userAPIKey, String request) {
        Log.d(TAG, "createUri starts");
        return "https://www.wanikani.com/api/user/" + userAPIKey + "/" + request;
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        mDownloadStatus = status;
        if(status == DownloadStatus.OK) {
            mRequestedStudyQueue = new RequestedStudyQueue();

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                mRequestedStudyQueue = objectMapper.readValue(data, RequestedStudyQueue.class);

            } catch (IOException e) {
                Log.e(TAG, "onDownloadComplete: Error in parsing Json with mapper" + e.getMessage() );
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
        }
        Log.d(TAG, "onDownloadComplete ends");
    }
}
