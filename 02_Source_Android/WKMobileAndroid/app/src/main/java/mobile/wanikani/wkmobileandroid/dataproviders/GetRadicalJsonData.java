package mobile.wanikani.wkmobileandroid.dataproviders;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import mobile.wanikani.wkmobileandroid.models.RequestedRadicalList;

public class GetRadicalJsonData extends AsyncTask<String, Void, RequestedRadicalList> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetJsonData";

    private RequestedRadicalList mRequestedRadicalList = null;
    private DownloadStatus mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;

    private final GetRadicalJsonData.OnDataAvailable mCallBack;

    public interface OnDataAvailable {
        void onDataAvailable(RequestedRadicalList data, DownloadStatus status);
    }

    public GetRadicalJsonData(GetRadicalJsonData.OnDataAvailable callBack) {
        Log.d(TAG, "GetRadicalJsonData called");
        mCallBack = callBack;
    }

    protected void onPostExecute(RequestedRadicalList vocabList) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(mRequestedRadicalList, mDownloadStatus);
        }
        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected RequestedRadicalList doInBackground(String... params) {
        Log.d(TAG, "doInBackground starts");
        String destinationUri =  createUri(params[0], ((params.length > 1)? params[1] : ""));

        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground ends");
        return mRequestedRadicalList;
    }

    private String createUri(String userAPIKey, String request) {
        Log.d(TAG, "createUri starts");
        return "https://www.wanikani.com/api/user/" + userAPIKey + "/" + request;
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        mDownloadStatus = status;
        if(status == DownloadStatus.OK) {
            mRequestedRadicalList = new RequestedRadicalList();

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                mRequestedRadicalList = objectMapper.readValue(data, RequestedRadicalList.class);

            } catch (IOException e) {
                Log.e(TAG, "onDownloadComplete: Error in parsing Json with mapper" + e.getMessage() );
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
        }
        Log.d(TAG, "onDownloadComplete ends");
    }
}
