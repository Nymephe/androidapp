package mobile.wanikani.wkmobileandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.wanikani.wkmobileandroid.dataproviders.DownloadStatus;
import mobile.wanikani.wkmobileandroid.dataproviders.GetKanjiJsonData;
import mobile.wanikani.wkmobileandroid.dataproviders.GetRadicalJsonData;
import mobile.wanikani.wkmobileandroid.dataproviders.GetVocabJsonData;
import mobile.wanikani.wkmobileandroid.models.Kanji;
import mobile.wanikani.wkmobileandroid.models.LearningMode;
import mobile.wanikani.wkmobileandroid.models.Radical;
import mobile.wanikani.wkmobileandroid.models.RequestedKanjiList;
import mobile.wanikani.wkmobileandroid.models.RequestedRadicalList;
import mobile.wanikani.wkmobileandroid.models.RequestedVocabList;
import mobile.wanikani.wkmobileandroid.models.Vocab;

public class DetailListActivity extends AppCompatActivity {

    private static final String TAG = "DetailListActivity";
    LearningMode mLearningMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aaaa);

        mLearningMode = (LearningMode) getIntent().getSerializableExtra(LearningMode.LEARNING_MODE);

        Fragment newFragment = DetailListFragment.getInstance(
                mLearningMode, (ArrayList<Integer>) getIntent().getSerializableExtra("SelectedLevels"));

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.linearLayout3, newFragment);
        transaction.commit();
    }
}
