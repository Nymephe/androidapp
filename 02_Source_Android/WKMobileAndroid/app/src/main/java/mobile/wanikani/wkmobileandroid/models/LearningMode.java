package mobile.wanikani.wkmobileandroid.models;

public enum LearningMode {
    LEVELS, // learn all from specific level
    RADICALS,
    KANJI,
    VOCAB,
    SHAMELIST;

    public static final String LEARNING_MODE = "LearningMode";
}