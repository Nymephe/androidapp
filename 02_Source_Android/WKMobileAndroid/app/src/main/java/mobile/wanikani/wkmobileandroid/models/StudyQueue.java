package mobile.wanikani.wkmobileandroid.models;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StudyQueue {

    @JsonProperty("lessons_available")
    private Integer lessonsAvailable;
    @JsonProperty("reviews_available")
    private Integer reviewsAvailable;
    @JsonProperty("next_review_date")
    private Object nextReviewDate;
    @JsonProperty("reviews_available_next_hour")
    private Integer reviewsAvailableNextHour;
    @JsonProperty("reviews_available_next_day")
    private Integer reviewsAvailableNextDay;

    public StudyQueue() {
    }

    @JsonProperty("lessons_available")
    public Integer getLessonsAvailable() {
        return lessonsAvailable;
    }

    @JsonProperty("lessons_available")
    public void setLessonsAvailable(Integer lessonsAvailable) {
        this.lessonsAvailable = lessonsAvailable;
    }

    @JsonProperty("reviews_available")
    public Integer getReviewsAvailable() {
        return reviewsAvailable;
    }

    @JsonProperty("reviews_available")
    public void setReviewsAvailable(Integer reviewsAvailable) {
        this.reviewsAvailable = reviewsAvailable;
    }

    @JsonProperty("next_review_date")
    public Object getNextReviewDate() {
        return nextReviewDate;
    }

    @JsonProperty("next_review_date")
    public void setNextReviewDate(Object nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    @JsonProperty("reviews_available_next_hour")
    public Integer getReviewsAvailableNextHour() {
        return reviewsAvailableNextHour;
    }

    @JsonProperty("reviews_available_next_hour")
    public void setReviewsAvailableNextHour(Integer reviewsAvailableNextHour) {
        this.reviewsAvailableNextHour = reviewsAvailableNextHour;
    }

    @JsonProperty("reviews_available_next_day")
    public Integer getReviewsAvailableNextDay() {
        return reviewsAvailableNextDay;
    }

    @JsonProperty("reviews_available_next_day")
    public void setReviewsAvailableNextDay(Integer reviewsAvailableNextDay) {
        this.reviewsAvailableNextDay = reviewsAvailableNextDay;
    }

}