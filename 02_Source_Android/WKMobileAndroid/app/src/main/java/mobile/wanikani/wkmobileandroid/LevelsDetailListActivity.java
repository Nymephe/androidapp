package mobile.wanikani.wkmobileandroid;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import mobile.wanikani.wkmobileandroid.models.LearningMode;

public class LevelsDetailListActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private LearningMode mLearningMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels_detail_list);

        mLearningMode = (LearningMode) getIntent().getSerializableExtra(LearningMode.LEARNING_MODE);

        ViewPagerAdapter adapter = null;

        if (LearningMode.SHAMELIST == mLearningMode) {
            adapter = new ViewPagerAdapter(
                    getSupportFragmentManager(), getApplicationContext(), null);
        } else {
            adapter = new ViewPagerAdapter(
                    getSupportFragmentManager(), getApplicationContext(),
                    (ArrayList<Integer>) getIntent().getSerializableExtra("SelectedLevels"));
        }

        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
}
