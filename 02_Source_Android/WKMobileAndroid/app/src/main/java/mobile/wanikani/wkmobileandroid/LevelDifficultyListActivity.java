package mobile.wanikani.wkmobileandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import mobile.wanikani.wkmobileandroid.models.LearningMode;
import mobile.wanikani.wkmobileandroid.models.LevelListItem;

public class LevelDifficultyListActivity extends AppCompatActivity implements RecyclerItemClickListener.OnRecyclerClickListener {

    private final ArrayList<String> mDifficultyLvls = new ArrayList (
            Arrays.asList(
                    "1-10 Pleasant",
                    "11-20 Painful",
                    "21-30 Death",
                    "31-40 Hell",
                    "41-50 Paradise",
                    "51-60 Reality"));

    private LevelsListRecyclerViewAdapter mlevelsListRecyclerViewAdapter;
    private LearningMode mSelectedLearningMode;
    private RecyclerView mListRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_difficulty_list);

        LearningMode result = (LearningMode) getIntent().getSerializableExtra(LearningMode.LEARNING_MODE);
        mSelectedLearningMode = (null != result) ? result : LearningMode.LEVELS; // just in case

        mListRecyclerView = findViewById(R.id.levels_difficulty_list);
        mListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mListRecyclerView, this));

        List<LevelListItem> lista = new ArrayList<>();
        ArrayList<String> lvlStrings = mDifficultyLvls;
        for (Integer idx = 0; idx < lvlStrings.size(); idx++)
        {
            lista.add( new LevelListItem(idx, lvlStrings.get(idx)));
        }

        mlevelsListRecyclerViewAdapter = new LevelsListRecyclerViewAdapter(this, lista);
        mListRecyclerView.setAdapter(mlevelsListRecyclerViewAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        CheckBox lvlCheckbox = view.findViewById(R.id.levels_checkbox);
        lvlCheckbox.setChecked(!lvlCheckbox.isChecked());
        mlevelsListRecyclerViewAdapter.updateStateList(lvlCheckbox.isChecked(), position);
    }

    public void onDifficultyOkClicked(View v) {

        ArrayList<LevelListItem> lista = new ArrayList<LevelListItem>();
        ArrayList<Integer> newlvlListInt = new ArrayList<>();

        for(Integer lvlId : mlevelsListRecyclerViewAdapter.getCheckedLevelsList()) {
            newlvlListInt.addAll(IntStream.range(((10 * (lvlId+1)) - 10) + 1, (10 * (lvlId+1)) + 1)
                    .boxed()
                    .collect(Collectors.toList()));
        }

        for (Integer i : newlvlListInt) {
            lista.add(new LevelListItem(i, Integer.toString(i)));
        }

        Intent intent = new Intent(this, LevelListActivity.class);
        intent.putExtra(LearningMode.LEARNING_MODE, mSelectedLearningMode);
        intent.putExtra(LevelListActivity.CUSTOM_LVL_LIST, lista);
        startActivity(intent);
        this.overridePendingTransition(0, 0);
        LevelDifficultyListActivity.this.finish();
    }

    public void onShowAllLevelsClicked(View v) {
        Intent intent = new Intent(this, LevelListActivity.class);
        intent.putExtra(LearningMode.LEARNING_MODE, mSelectedLearningMode);
        startActivity(intent);
        this.overridePendingTransition(0, 0);
        LevelDifficultyListActivity.this.finish();
    }
}
