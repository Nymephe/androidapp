package mobile.wanikani.wkmobileandroid.settings;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import mobile.wanikani.wkmobileandroid.MainActivity;
import mobile.wanikani.wkmobileandroid.R;

public class NotificationReceiver extends BroadcastReceiver {
    private static final String TAG = "NotificationReceiver";
    private final String mChannelId = "WANIKANI_CHN_ID";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: starts");

        Intent intentToRepeat = new Intent(context, MainActivity.class);
        intentToRepeat.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        context,
                        SettingsSetTimeDialog.ALARM_REQUEST_CODE,
                        intentToRepeat,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        Notification repeatedNotification = getNotification(context, pendingIntent);

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(SettingsSetTimeDialog.ALARM_REQUEST_CODE, repeatedNotification);
    }

    private Notification getNotification(Context context, PendingIntent pendingIntent) {
        Log.d(TAG, "createNotification: starts");
        createNotificationChannel(context);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, mChannelId)
                .setSmallIcon(R.drawable.logosite)
                .setContentTitle("WaniKani Mobile App!")
                .setContentText("It's study time! Go and learn. It's for your future.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        return builder.build();
    }

    private void createNotificationChannel(Context context) {
        Log.d(TAG, "createNotificationChannel: starts");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name   = context.getString(R.string.channel_name);
            String description  = context.getString(R.string.channel_description);
            int importance      = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(mChannelId, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
