package mobile.wanikani.wkmobileandroid.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import static android.content.Context.MODE_PRIVATE;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vocab extends BasicData implements Serializable {

    final public static String SHAME_LIST = "VocabShamelist";

    @JsonProperty("level")
    private Integer level;
    @JsonProperty("character")
    private String character;
    @JsonProperty("kana")
    private String kana;
    @JsonProperty("meaning")
    private String meaning;
    @JsonProperty("user_specific")
    private UserSpecific userSpecific;

    public Vocab() {
    }

    @JsonProperty("level")
    public Integer getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(Integer level) {
        this.level = level;
    }

    @JsonProperty("character")
    public String getCharacter() {
        return character;
    }

    @JsonProperty("character")
    public void setCharacter(String character) {
        this.character = character;
    }

    @JsonProperty("kana")
    public String getKana() {
        return kana;
    }

    @JsonProperty("kana")
    public void setKana(String kana) {
        this.kana = kana;
    }

    @JsonProperty("meaning")
    public String getMeaning() {
        return meaning;
    }

    @JsonProperty("meaning")
    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    @JsonProperty("user_specific")
    public UserSpecific getUserSpecific() {
        return userSpecific;
    }

    @JsonProperty("user_specific")
    public void setUserSpecific(UserSpecific userSpecific) {
        this.userSpecific = userSpecific;
    }

    public boolean isItemOnShamelist(Context context) {
        SharedPreferences vocabShamelist = context.getSharedPreferences(SHAME_LIST, MODE_PRIVATE);
        SharedPreferences.Editor editor = vocabShamelist.edit();
        return (null != vocabShamelist.getString(getCharacter(), null));
    }

    static public Vocab getDummyVocab() {
        return new Vocab() {
            {
                setCharacter("Please wait");
                setMeaning("");
            }
        };
    }
}