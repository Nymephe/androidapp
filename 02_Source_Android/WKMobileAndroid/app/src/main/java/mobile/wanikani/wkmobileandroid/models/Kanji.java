package mobile.wanikani.wkmobileandroid.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import static android.content.Context.MODE_PRIVATE;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Kanji extends BasicData implements Serializable {

    final public static String SHAME_LIST = "KanjiShamelist";

    @JsonProperty("level")
    private Integer level;
    @JsonProperty("character")
    private String character;
    @JsonProperty("meaning")
    private String meaning;
    @JsonProperty("onyomi")
    private String onyomi;
    @JsonProperty("kunyomi")
    private String kunyomi;
    @JsonProperty("important_reading")
    private String importantReading;
    @JsonProperty("nanori")
    private String nanori;
    @JsonProperty("user_specific")
    private UserSpecific userSpecific;

    public Kanji() {
    }

    @JsonProperty("level")
    public Integer getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(Integer level) {
        this.level = level;
    }

    @JsonProperty("character")
    public String getCharacter() {
        return character;
    }

    @JsonProperty("character")
    public void setCharacter(String character) {
        this.character = character;
    }

    @JsonProperty("meaning")
    public String getMeaning() {
        return meaning;
    }

    @JsonProperty("meaning")
    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    @JsonProperty("onyomi")
    public String getOnyomi() {
        return onyomi;
    }

    @JsonProperty("onyomi")
    public void setOnyomi(String onyomi) {
        this.onyomi = onyomi;
    }

    @JsonProperty("kunyomi")
    public String getKunyomi() {
        return kunyomi;
    }

    @JsonProperty("kunyomi")
    public void setKunyomi(String kunyomi) {
        this.kunyomi = kunyomi;
    }

    @JsonProperty("important_reading")
    public String getImportantReading() {
        return importantReading;
    }

    @JsonProperty("important_reading")
    public void setImportantReading(String importantReading) {
        this.importantReading = importantReading;
    }

    @JsonProperty("nanori")
    public String getNanori() {
        return nanori;
    }

    @JsonProperty("nanori")
    public void setNanori(String nanori) {
        this.nanori = nanori;
    }

    @JsonProperty("user_specific")
    public UserSpecific getUserSpecific() {
        return userSpecific;
    }

    @JsonProperty("user_specific")
    public void setUserSpecific(UserSpecific userSpecific) {
        this.userSpecific = userSpecific;
    }

    public boolean isItemOnShamelist(Context context) {
        SharedPreferences radicalShamelist = context.getSharedPreferences(SHAME_LIST, MODE_PRIVATE);
        SharedPreferences.Editor editor = radicalShamelist.edit();
        return (null != radicalShamelist.getString(getCharacter(), null));
    }

    public static Kanji getDummyKanji() {
        return new Kanji() {
            {
                setCharacter("Please wait");
                setMeaning("");
                setOnyomi("");
                setKunyomi("");
                setNanori("");
            }
        };
    }
}