package mobile.wanikani.wkmobileandroid.dataproviders;

import android.os.AsyncTask;
import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import mobile.wanikani.wkmobileandroid.models.RequestedKanjiList;

public class GetKanjiJsonData extends AsyncTask<String, Void, RequestedKanjiList> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetKanjiJsonData";

    private RequestedKanjiList mRequestedKanjiList = null;
    private DownloadStatus mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;

    private final GetKanjiJsonData.OnDataAvailable mCallBack;

    public interface OnDataAvailable {
        void onDataAvailable(RequestedKanjiList data, DownloadStatus status);
    }

    public GetKanjiJsonData(GetKanjiJsonData.OnDataAvailable callBack) {
        Log.d(TAG, "GetKanjiJsonData called");
        mCallBack = callBack;
    }

    protected void onPostExecute(RequestedKanjiList vocabList) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(mRequestedKanjiList, mDownloadStatus);
        }
        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected RequestedKanjiList doInBackground(String... params) {
        Log.d(TAG, "doInBackground starts");
        String destinationUri =  createUri(params[0], ((params.length > 1)? params[1] : ""));

        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground ends");
        return mRequestedKanjiList;
    }

    private String createUri(String userAPIKey, String request) {
        Log.d(TAG, "createUri starts");
        return "https://www.wanikani.com/api/user/" + userAPIKey + "/" + request;
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        mDownloadStatus = status;
        if(status == DownloadStatus.OK) {
            mRequestedKanjiList = new RequestedKanjiList();

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                mRequestedKanjiList = objectMapper.readValue(data, RequestedKanjiList.class);

            } catch (IOException e) {
                Log.e(TAG, "onDownloadComplete: Error in parsing Json with mapper" + e.getMessage() );
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
        }
        Log.d(TAG, "onDownloadComplete ends");
    }
}