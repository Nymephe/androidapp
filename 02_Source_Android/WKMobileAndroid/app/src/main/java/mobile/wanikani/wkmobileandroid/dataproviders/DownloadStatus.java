package mobile.wanikani.wkmobileandroid.dataproviders;

public enum DownloadStatus {
    IDLE,
    PROCESSING,
    NOT_INITIALISED,
    FAILED_OR_EMPTY,
    OK
}
