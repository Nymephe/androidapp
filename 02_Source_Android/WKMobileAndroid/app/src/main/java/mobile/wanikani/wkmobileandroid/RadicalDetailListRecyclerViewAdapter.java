package mobile.wanikani.wkmobileandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mobile.wanikani.wkmobileandroid.models.Radical;

import static android.content.Context.MODE_PRIVATE;

public class RadicalDetailListRecyclerViewAdapter extends RecyclerView.Adapter<RadicalDetailListRecyclerViewAdapter.RadicalListDetailItemViewHolder> {

    private ArrayList<Radical> mRadicalList;
    private Context mContext;

    public RadicalDetailListRecyclerViewAdapter(Context context, ArrayList<Radical> items) {
        this.mContext = context;
        this.mRadicalList = items;
    }

    @NonNull
    @Override
    public RadicalDetailListRecyclerViewAdapter.RadicalListDetailItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.radical_detail_list_item, parent, false);
        return new RadicalDetailListRecyclerViewAdapter.RadicalListDetailItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RadicalDetailListRecyclerViewAdapter.RadicalListDetailItemViewHolder holder, int position) {
        holder.idx.setText(Integer.toString(position + 1 ));

        bindCharacter(holder, position);

        String meaning = mRadicalList.get(position).getMeaning();
        if (null != mRadicalList.get(position).getUserSpecific() &&
                null != mRadicalList.get(position).getUserSpecific().getUserSynonyms()) {
            meaning += ", " + TextUtils.join(", ", mRadicalList.get(position).getUserSpecific().getUserSynonyms());
        }
        holder.meaning.setText(meaning);

        if (mRadicalList.get(position).isItemOnShamelist(mContext)) {
            holder.shameImage.setImageResource(R.drawable.emoji_red);
        } else {
            holder.shameImage.setImageResource(R.drawable.emoji_gray);
        }
    }

    @Override
    public int getItemCount() {
        return ((mRadicalList != null) && (mRadicalList.size() !=0) ? mRadicalList.size() : 0);
    }

    private void bindCharacter(RadicalDetailListRecyclerViewAdapter.RadicalListDetailItemViewHolder holder, int position) {

        String character = mRadicalList.get(position).getCharacter();
        if (null != character && false == character.equalsIgnoreCase("None") && false == character.isEmpty()) {
            holder.characterText.setVisibility(View.VISIBLE);
            holder.characterText.setText(character);
            holder.characterImage.setVisibility(View.GONE);
        } else {
            holder.characterText.setVisibility(View.GONE);
            bindCharacterImg(holder, position);
        }
    }

    private void bindCharacterImg(RadicalDetailListRecyclerViewAdapter.RadicalListDetailItemViewHolder holder, int position) {
        String characterImageUrl = mRadicalList.get(position).getImage();
        if (null != characterImageUrl && false == characterImageUrl.equalsIgnoreCase("None") && false == characterImageUrl.isEmpty()) {
            holder.characterImage.setVisibility(View.VISIBLE);

            Picasso.get().load(characterImageUrl)
                    .error(R.drawable.cross_icon)
                    .into(holder.characterImage);
        }
    }

    public void loadNewData(List<Radical> newItemList) {
        mRadicalList = new ArrayList<Radical>(newItemList);
        notifyDataSetChanged();
    }

    static class RadicalListDetailItemViewHolder extends RecyclerView.ViewHolder {
        ImageView characterImage;
        TextView characterText;
        TextView meaning;
        ImageView shameImage;
        TextView idx;

        public RadicalListDetailItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.characterText = itemView.findViewById(R.id.radical_detail_char);
            this.characterImage = itemView.findViewById(R.id.radical_detail_char_image);
            this.characterImage.setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY);
            this.meaning = itemView.findViewById(R.id.radical_detail_meaning);
            this.shameImage = itemView.findViewById(R.id.radical_detail_image);
            this.idx = itemView.findViewById(R.id.radical_detail_number);
        }
    }
}
