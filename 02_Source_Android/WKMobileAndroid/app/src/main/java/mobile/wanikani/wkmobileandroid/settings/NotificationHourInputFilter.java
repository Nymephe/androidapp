package mobile.wanikani.wkmobileandroid.settings;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

enum HourSystem {
    HOURS_12(0),
    HOURS_24(1);

    private static Map mMap = new HashMap<>();
    private int mValue;

    HourSystem(int value) {
        this.mValue = value;
    }

    static {
        for (HourSystem hourSystem : HourSystem.values()) {
            mMap.put(hourSystem.mValue, hourSystem);
        }
    }

    public static HourSystem valueOf(int hourSystem) {
        return (HourSystem) mMap.get(hourSystem);
    }

    public int getValue() {
        return mValue;
    }
}

public class NotificationHourInputFilter implements InputFilter {

    private static final String TAG = "NotificationHourInputFi";
    private final int mMinValue = 0;
    private int mMaxValue;

    public NotificationHourInputFilter(int max) {
        this.mMaxValue = max;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            String inputString = dest.toString() + source.toString();
            if (isInRange(Integer.parseInt(inputString)) || 0 == inputString.length())
                return null;
        } catch (NumberFormatException e) {
            Log.e(TAG, "filter: Error while executing notification hour filter" + e.getMessage());
        }
        return "";
    }

    private boolean isInRange(int input) {
        return (input <= mMaxValue && input >= mMinValue);
    }
}
