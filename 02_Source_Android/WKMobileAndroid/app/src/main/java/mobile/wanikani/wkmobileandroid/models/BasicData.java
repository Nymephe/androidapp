package mobile.wanikani.wkmobileandroid.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BasicData {

    private static final String TAG = "BasicData";

    public boolean isItemOnShamelist(Context context){ return false;}

    public boolean updateShamelistItem(SharedPreferences sharedPreferences, SharedPreferences.Editor editor, String idString, Context context) {
        boolean result = false;

        if (isItemOnShamelist(context)) {
            editor.remove(idString);
            result = false;
        } else {
            ObjectMapper mapper = new ObjectMapper();

            try {
                editor.putString(idString, mapper.writeValueAsString(this));
                result = true;
            } catch (JsonProcessingException e) {
                Log.d(TAG, "onItemClick: JsonProcessingException occurred");
            }
        }
        editor.apply();

        return result;
    }
}
