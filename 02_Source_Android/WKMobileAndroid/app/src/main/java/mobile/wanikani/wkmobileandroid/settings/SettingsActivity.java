package mobile.wanikani.wkmobileandroid.settings;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import mobile.wanikani.wkmobileandroid.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        loadSettings();
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveSettings();
    }

    public void onRemoveShamelistClicked(View view) {
        SettingsRemoveShamelistDialog dialog = new SettingsRemoveShamelistDialog(SettingsActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void onSetNotificationClicked(View view) {
        SettingsSetTimeDialog dialog = new SettingsSetTimeDialog(SettingsActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void loadSettings() {
        SharedPreferences settings = getSharedPreferences("AppSettings", MODE_PRIVATE);

        Switch nightModeSwitch = findViewById(R.id.night_mode_switch);
        nightModeSwitch.setChecked(settings.getBoolean(getString(R.string.settings_night_mode),false));

        RadioGroup settingsReading = findViewById(R.id.settings_reading);
        RadioButton buttonToCheck = (RadioButton) settingsReading.getChildAt(
                settings.getInt(getString(R.string.settings_reading_with), 0));
        buttonToCheck.setChecked(true);

        RadioGroup settingsShowFirst = findViewById(R.id.settings_show_first);
        buttonToCheck = (RadioButton) settingsShowFirst.getChildAt(
                settings.getInt(getString(R.string.settings_show_first), 0));
        buttonToCheck.setChecked(true);
    }

    private void saveSettings() {
        SharedPreferences settings = getSharedPreferences("AppSettings", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        Switch nightModeSwitch = findViewById(R.id.night_mode_switch);
        editor.putBoolean(getString(R.string.settings_night_mode), nightModeSwitch.isChecked());

        // first buttons in RadioGroup. If first was checked then save 0 to settings, if not then 1
        RadioButton readingButton = findViewById(R.id.radio_reading_with_character);
        editor.putInt(getString(R.string.settings_reading_with), readingButton.isChecked() ? 0 : 1);

        RadioButton showButton = findViewById(R.id.radio_show_first_character);
        editor.putInt(getString(R.string.settings_show_first), showButton.isChecked() ? 0 : 1);

        editor.apply();
    }
}
