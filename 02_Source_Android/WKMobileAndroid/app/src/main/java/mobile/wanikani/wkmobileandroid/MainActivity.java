package mobile.wanikani.wkmobileandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import mobile.wanikani.wkmobileandroid.dataproviders.DownloadStatus;
import mobile.wanikani.wkmobileandroid.dataproviders.GetJsonData;
import mobile.wanikani.wkmobileandroid.models.LearningMode;
import mobile.wanikani.wkmobileandroid.models.RequestedStudyQueue;
import mobile.wanikani.wkmobileandroid.models.UserInformation;
import mobile.wanikani.wkmobileandroid.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   GetJsonData.OnDataAvailable {

    private static final String TAG = "MainActivity";
    private final String mRequest = "study-queue";
    private SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Log.d(TAG, "onCreate: called");

        mSettings = getSharedPreferences("AppSettings", MODE_PRIVATE);

        GetJsonData getJsonData = new GetJsonData(this);
        getJsonData.execute(mSettings.getString(getString(R.string.login_api_key),null), mRequest);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_levels) {
            Intent intent = new Intent( this, LevelDifficultyListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, LearningMode.LEVELS);
            startActivity(intent);
        } else if (id == R.id.nav_radicals) {
            Intent intent = new Intent( this, LevelDifficultyListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, LearningMode.RADICALS);
            startActivity(intent);
        } else if (id == R.id.nav_kanji) {
            Intent intent = new Intent( this, LevelDifficultyListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, LearningMode.KANJI);
            startActivity(intent);
        } else if (id == R.id.nav_vocab) {
            Intent intent = new Intent( this, LevelDifficultyListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, LearningMode.VOCAB);
            startActivity(intent);
        } else if (id == R.id.nav_shamelist) {
            Intent intent = new Intent( this, LevelsDetailListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, LearningMode.SHAMELIST);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDataAvailable(RequestedStudyQueue data, DownloadStatus status) {
        UserInformation userInformation = data.getUserInformation();

        Log.d(TAG, "onStudyQueueDataAvailable: ");
        TextView username = findViewById(R.id.username);
        username.setText(userInformation.getUsername());

        TextView level = findViewById(R.id.current_lvl_data);
        level.setText(Integer.toString(userInformation.getLevel()));

        TextView inReview = findViewById(R.id.in_review_data);
        inReview.setText(Integer.toString(data.getStudyQueue().getReviewsAvailable()));

        TextView toLearn = findViewById(R.id.to_learn_data);
        toLearn.setText(Integer.toString(data.getStudyQueue().getLessonsAvailable()));
    }
}
