package mobile.wanikani.wkmobileandroid.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class RequestedKanjiList {

    @JsonProperty("user_information")
    private UserInformation userInformation;
    @JsonProperty("requested_information")
    private List<Kanji> kanjiList = null;

    public RequestedKanjiList() {
    }

    @JsonProperty("user_information")
    public UserInformation getUserInformation() {
        return userInformation;
    }

    @JsonProperty("user_information")
    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    @JsonProperty("requested_information")
    public List<Kanji> getRequestedInformation() {
        return kanjiList;
    }

    @JsonProperty("requested_information")
    public void setRequestedInformation(List<Kanji> requestedInformation) {
        this.kanjiList = requestedInformation;
    }
}
