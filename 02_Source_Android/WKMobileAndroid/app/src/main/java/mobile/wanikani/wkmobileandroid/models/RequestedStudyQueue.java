package mobile.wanikani.wkmobileandroid.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestedStudyQueue {

    @JsonProperty("user_information")
    private UserInformation userInformation;
    @JsonProperty("requested_information")
    private StudyQueue studyQueue;

    public RequestedStudyQueue() {
    }

    @JsonProperty("user_information")
    public UserInformation getUserInformation() {
        return userInformation;
    }

    @JsonProperty("user_information")
    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    @JsonProperty("requested_information")
    public StudyQueue getStudyQueue() {
        return studyQueue;
    }

    @JsonProperty("requested_information")
    public void setStudyQueue(StudyQueue requestedInformation) {
        this.studyQueue = requestedInformation;
    }

}