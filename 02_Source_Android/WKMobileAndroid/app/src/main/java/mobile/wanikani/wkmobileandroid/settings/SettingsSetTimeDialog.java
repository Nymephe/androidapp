package mobile.wanikani.wkmobileandroid.settings;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import mobile.wanikani.wkmobileandroid.R;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class SettingsSetTimeDialog extends Dialog implements
        android.view.View.OnClickListener {
    private static final String TAG = "SettingsSetTimeDialog";
    public static int ALARM_REQUEST_CODE = 100;

    private SharedPreferences mSettings;
    private final int mMaxMinutes = 59;
    private final int mMaxHour24 = 23;
    private final int mMaxHour12 = 12;

    private Button mOkButton;
    private TextView mNotificationHour;
    private TextView mNotificationMinutes;
    private Switch mNotificationEnable;

    public SettingsSetTimeDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settings_notifier_time);

        mNotificationHour = findViewById(R.id.notification_hours);
        mNotificationMinutes = findViewById(R.id.notification_minutes);
        mNotificationEnable = findViewById(R.id.notification_enable);
        mOkButton = findViewById(R.id.settings_ok_button);
        mOkButton.setOnClickListener(this);

        mSettings = getContext().getSharedPreferences("AppSettings", MODE_PRIVATE);

        loadSettings();

        int hourSystem = mSettings.getInt(
                getContext().getString(
                        R.string.settings_notification_hour_system),
                HourSystem.HOURS_24.getValue());

        mNotificationHour.setFilters( new InputFilter[] {
                new NotificationHourInputFilter((HourSystem.HOURS_12 == HourSystem.valueOf(hourSystem) ? mMaxHour12 : mMaxHour24))
        });

        mNotificationMinutes.setFilters( new InputFilter[] {
                new NotificationHourInputFilter(mMaxMinutes)
        });
    }

    @Override
    public void onClick(View v) {
        saveSettings();
        dismiss();
    }

    private void loadSettings() {
        mNotificationHour.setText(
                String.format(Locale.getDefault(), "%02d",
                        mSettings.getInt(
                                getContext().getString(R.string.settings_notification_hour), 12)));
        mNotificationMinutes.setText(
                String.format(Locale.getDefault(), "%02d",
                        mSettings.getInt(
                                getContext().getString(R.string.settings_notification_minutes), 00)));

        mNotificationEnable.setChecked(
                mSettings.getBoolean(getContext().getString(R.string.settings_notification_enable), false));
    }

    private void saveSettings() {
        SharedPreferences.Editor editor = mSettings.edit();

        String hourString = mNotificationHour.getText().toString();
        String minutesString = mNotificationMinutes.getText().toString();

        // Proceed only if both input fields were set
        if (0 != hourString.length() && 0 != minutesString.length()) {
            editor.putInt(getContext().getString(
                    R.string.settings_notification_hour),
                    Integer.parseInt(hourString));

            editor.putInt(getContext().getString(
                    R.string.settings_notification_minutes),
                    Integer.parseInt(minutesString));

            editor.putBoolean(getContext().getString(
                    R.string.settings_notification_enable),
                    mNotificationEnable.isChecked());

            editor.apply();

            if (true == mNotificationEnable.isChecked()) {
                registerAlarmBroadcast(Integer.parseInt(hourString), Integer.parseInt(minutesString));
            } else {
                cancelAlarmRTC();
            }
        }
    }

    private void registerAlarmBroadcast(int hour, int min) {
        Log.d(TAG, "registerAlarmBroadcast: ");
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);

        Intent intent = new Intent(getContext(), NotificationReceiver.class);
        PendingIntent mAlarmIntent = PendingIntent.getBroadcast(
                getContext(), ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)getContext().getSystemService(ALARM_SERVICE);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,  mAlarmIntent);
    }
    public void cancelAlarmRTC() {
        Log.d(TAG, "cancelAlarmRTC: started");
        AlarmManager alarmManager = (AlarmManager)getContext().getSystemService(ALARM_SERVICE);
        if (null != alarmManager) {
            Log.d(TAG, "cancelAlarmRTC: cancelled");
            Intent intent = new Intent(getContext(), NotificationReceiver.class);
            alarmManager.cancel(PendingIntent.getBroadcast(
                    getContext(), ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        }
    }
}
