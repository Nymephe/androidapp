package mobile.wanikani.wkmobileandroid.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInformation {
    @JsonProperty("username")
    private String username;
    @JsonProperty("gravatar")
    private String gravatar;
    @JsonProperty("level")
    private Integer level;
    @JsonProperty("title")
    private String title;
    @JsonProperty("about")
    private String about;
    @JsonProperty("website")
    private String website;
    @JsonProperty("twitter")
    private String twitter;
    @JsonProperty("topics_count")
    private Integer topicsCount;
    @JsonProperty("posts_count")
    private Integer postsCount;
    @JsonProperty("creation_date")
    private Integer creationDate;
    @JsonProperty("vacation_date")
    private Integer vacationDate;

    public UserInformation() {
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("gravatar")
    public String getGravatar() {
        return gravatar;
    }

    @JsonProperty("gravatar")
    public void setGravatar(String gravatar) {
        this.gravatar = gravatar;
    }

    @JsonProperty("level")
    public Integer getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(Integer level) {
        this.level = level;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("about")
    public String getAbout() {
        return about;
    }

    @JsonProperty("about")
    public void setAbout(String about) {
        this.about = about;
    }

    @JsonProperty("website")
    public String getWebsite() {
        return website;
    }

    @JsonProperty("website")
    public void setWebsite(String website) {
        this.website = website;
    }

    @JsonProperty("twitter")
    public String getTwitter() {
        return twitter;
    }

    @JsonProperty("twitter")
    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    @JsonProperty("topics_count")
    public Integer getTopicsCount() {
        return topicsCount;
    }

    @JsonProperty("topics_count")
    public void setTopicsCount(Integer topicsCount) {
        this.topicsCount = topicsCount;
    }

    @JsonProperty("posts_count")
    public Integer getPostsCount() {
        return postsCount;
    }

    @JsonProperty("posts_count")
    public void setPostsCount(Integer postsCount) {
        this.postsCount = postsCount;
    }

    @JsonProperty("creation_date")
    public Integer getCreationDate() {
        return creationDate;
    }

    @JsonProperty("creation_date")
    public void setCreationDate(Integer creationDate) {
        this.creationDate = creationDate;
    }

    @JsonProperty("vacation_date")
    public Integer getVacationDate() {
        return vacationDate;
    }

    @JsonProperty("vacation_date")
    public void setVacationDate(Integer vacationDate) {
        this.vacationDate = vacationDate;
    }
}