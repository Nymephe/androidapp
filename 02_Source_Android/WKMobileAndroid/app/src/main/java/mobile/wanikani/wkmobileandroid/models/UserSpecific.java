package mobile.wanikani.wkmobileandroid.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class UserSpecific implements Serializable {

    @JsonProperty("srs")
    private String srs;
    @JsonProperty("srs_numeric")
    private Integer srsNumeric;
    @JsonProperty("unlocked_date")
    private Integer unlockedDate;
    @JsonProperty("available_date")
    private Integer availableDate;
    @JsonProperty("burned")
    private Boolean burned;
    @JsonProperty("burned_date")
    private Integer burnedDate;
    @JsonProperty("meaning_correct")
    private Integer meaningCorrect;
    @JsonProperty("meaning_incorrect")
    private Integer meaningIncorrect;
    @JsonProperty("meaning_max_streak")
    private Integer meaningMaxStreak;
    @JsonProperty("meaning_current_streak")
    private Integer meaningCurrentStreak;
    @JsonProperty("reading_correct")
    private Integer readingCorrect;
    @JsonProperty("reading_incorrect")
    private Integer readingIncorrect;
    @JsonProperty("reading_max_streak")
    private Integer readingMaxStreak;
    @JsonProperty("reading_current_streak")
    private Integer readingCurrentStreak;
    @JsonProperty("meaning_note")
    private String meaningNote;
    @JsonProperty("reading_note")
    private String readingNote;
    @JsonProperty("user_synonyms")
    private String[] userSynonyms;

    public UserSpecific() {
    }

    @JsonProperty("srs")
    public String getSrs() {
        return srs;
    }

    @JsonProperty("srs")
    public void setSrs(String srs) {
        this.srs = srs;
    }

    @JsonProperty("srs_numeric")
    public Integer getSrsNumeric() {
        return srsNumeric;
    }

    @JsonProperty("srs_numeric")
    public void setSrsNumeric(Integer srsNumeric) {
        this.srsNumeric = srsNumeric;
    }

    @JsonProperty("unlocked_date")
    public Integer getUnlockedDate() {
        return unlockedDate;
    }

    @JsonProperty("unlocked_date")
    public void setUnlockedDate(Integer unlockedDate) {
        this.unlockedDate = unlockedDate;
    }

    @JsonProperty("available_date")
    public Integer getAvailableDate() {
        return availableDate;
    }

    @JsonProperty("available_date")
    public void setAvailableDate(Integer availableDate) {
        this.availableDate = availableDate;
    }

    @JsonProperty("burned")
    public Boolean getBurned() {
        return burned;
    }

    @JsonProperty("burned")
    public void setBurned(Boolean burned) {
        this.burned = burned;
    }

    @JsonProperty("burned_date")
    public Integer getBurnedDate() {
        return burnedDate;
    }

    @JsonProperty("burned_date")
    public void setBurnedDate(Integer burnedDate) {
        this.burnedDate = burnedDate;
    }

    @JsonProperty("meaning_correct")
    public Integer getMeaningCorrect() {
        return meaningCorrect;
    }

    @JsonProperty("meaning_correct")
    public void setMeaningCorrect(Integer meaningCorrect) {
        this.meaningCorrect = meaningCorrect;
    }

    @JsonProperty("meaning_incorrect")
    public Integer getMeaningIncorrect() {
        return meaningIncorrect;
    }

    @JsonProperty("meaning_incorrect")
    public void setMeaningIncorrect(Integer meaningIncorrect) {
        this.meaningIncorrect = meaningIncorrect;
    }

    @JsonProperty("meaning_max_streak")
    public Integer getMeaningMaxStreak() {
        return meaningMaxStreak;
    }

    @JsonProperty("meaning_max_streak")
    public void setMeaningMaxStreak(Integer meaningMaxStreak) {
        this.meaningMaxStreak = meaningMaxStreak;
    }

    @JsonProperty("meaning_current_streak")
    public Integer getMeaningCurrentStreak() {
        return meaningCurrentStreak;
    }

    @JsonProperty("meaning_current_streak")
    public void setMeaningCurrentStreak(Integer meaningCurrentStreak) {
        this.meaningCurrentStreak = meaningCurrentStreak;
    }

    @JsonProperty("reading_correct")
    public Integer getReadingCorrect() {
        return readingCorrect;
    }

    @JsonProperty("reading_correct")
    public void setReadingCorrect(Integer readingCorrect) {
        this.readingCorrect = readingCorrect;
    }

    @JsonProperty("reading_incorrect")
    public Integer getReadingIncorrect() {
        return readingIncorrect;
    }

    @JsonProperty("reading_incorrect")
    public void setReadingIncorrect(Integer readingIncorrect) {
        this.readingIncorrect = readingIncorrect;
    }

    @JsonProperty("reading_max_streak")
    public Integer getReadingMaxStreak() {
        return readingMaxStreak;
    }

    @JsonProperty("reading_max_streak")
    public void setReadingMaxStreak(Integer readingMaxStreak) {
        this.readingMaxStreak = readingMaxStreak;
    }

    @JsonProperty("reading_current_streak")
    public Integer getReadingCurrentStreak() {
        return readingCurrentStreak;
    }

    @JsonProperty("reading_current_streak")
    public void setReadingCurrentStreak(Integer readingCurrentStreak) {
        this.readingCurrentStreak = readingCurrentStreak;
    }

    @JsonProperty("meaning_note")
    public String getMeaningNote() {
        return meaningNote;
    }

    @JsonProperty("meaning_note")
    public void setMeaningNote(String meaningNote) {
        this.meaningNote = meaningNote;
    }

    @JsonProperty("reading_note")
    public String getReadingNote() {
        return readingNote;
    }

    @JsonProperty("reading_note")
    public void setReadingNote(String readingNote) {
        this.readingNote = readingNote;
    }

    @JsonProperty("user_synonyms")
    public String[] getUserSynonyms() {
        return userSynonyms;
    }

    @JsonProperty("user_synonyms")
    public void setUserSynonyms(String[] userSynonyms) {
        this.userSynonyms = userSynonyms;
    }

}