package mobile.wanikani.wkmobileandroid.settings;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import java.io.File;

import mobile.wanikani.wkmobileandroid.R;
import mobile.wanikani.wkmobileandroid.models.Kanji;
import mobile.wanikani.wkmobileandroid.models.Radical;
import mobile.wanikani.wkmobileandroid.models.Vocab;

public class SettingsRemoveShamelistDialog extends Dialog {

    Activity mActivity;
    private Button mShamelistNoButton;
    private Button mShamelistYesButton;

    public SettingsRemoveShamelistDialog(Activity activity) {
        super(activity);
        mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settings_remove_shamelist);

        mShamelistNoButton = findViewById(R.id.shamelist_no_button);
        mShamelistYesButton = findViewById(R.id.shamelist_yes_button);

        mShamelistNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mShamelistYesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File dir = new File(getContext().getFilesDir().getParent() + "/shared_prefs/");
                String[] shameLists = new String[] {
                        Kanji.SHAME_LIST,
                        Vocab.SHAME_LIST,
                        Radical.SHAME_LIST
                };
                for (int i = 0; i < shameLists.length; i++) {
                    // clear each of the prefrances
                    getContext().getSharedPreferences(
                            shameLists[i].replace(".xml", ""),
                            getContext().MODE_PRIVATE).edit().clear().commit();
                }

                // Make sure it has enough time to save all the commited changes
                try { Thread.sleep(1000); } catch (InterruptedException e) {}
                for (String shameList : shameLists) {
                    // delete the files
                    new File(dir, shameList).delete();
                }

                dismiss();
            }
        });
    }
}
