package mobile.wanikani.wkmobileandroid.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class RequestedVocabList {
    @JsonProperty("user_information")
    private UserInformation userInformation;
    @JsonProperty("requested_information")
    private List<Vocab> requestedInformation = null;

    public RequestedVocabList() {
    }

    @JsonProperty("user_information")
    public UserInformation getUserInformation() {
        return userInformation;
    }

    @JsonProperty("user_information")
    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    @JsonProperty("requested_information")
    public List<Vocab> getRequestedInformation() {
        return requestedInformation;
    }

    @JsonProperty("requested_information")
    public void setRequestedInformation(List<Vocab> requestedInformation) {
        this.requestedInformation = requestedInformation;
    }

}
