package mobile.wanikani.wkmobileandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mobile.wanikani.wkmobileandroid.models.BasicData;
import mobile.wanikani.wkmobileandroid.models.Kanji;
import mobile.wanikani.wkmobileandroid.models.LearningMode;
import mobile.wanikani.wkmobileandroid.models.Radical;
import mobile.wanikani.wkmobileandroid.models.Vocab;

enum LearningScreenType {
    CHARACTER,
    INFO,
}

public class LearningActivity extends AppCompatActivity {

    LearningScreenType mCurrentLearningScreenType;
    LearningScreenType mLearningScreenType;
    int mReadingWith;
    LearningMode mLearningMode;
    ArrayList<Radical> mRadicalList;
    ArrayList<Kanji> mKanjiList;
    ArrayList<Vocab> mVocabList;
    BasicData currentItem;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning);

        mLearningMode = (LearningMode) getIntent().getSerializableExtra(LearningMode.LEARNING_MODE);
        mKanjiList = (ArrayList<Kanji>) getIntent().getSerializableExtra("kanji_list");
        mRadicalList = (ArrayList<Radical>) getIntent().getSerializableExtra("radicals_list");
        mVocabList = (ArrayList<Vocab>) getIntent().getSerializableExtra("vocab_list");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        SharedPreferences settings = getSharedPreferences("AppSettings", MODE_PRIVATE);
        settings.getInt(getString(R.string.settings_reading_with), 0);

        if (0 == settings.getInt(getString(R.string.settings_show_first), 0)) {
            mLearningScreenType = LearningScreenType.CHARACTER;
        } else {
            mLearningScreenType = LearningScreenType.INFO;
        }
        mCurrentLearningScreenType = mLearningScreenType;

        mReadingWith = settings.getInt(getString(R.string.settings_reading_with), 0);

    }
    public int getReadingWith() { return mReadingWith; }
    public LearningScreenType getLearningScreenType() { return mLearningScreenType; }
    public void setLearningScreenType(LearningScreenType learningScreenType) { mCurrentLearningScreenType = learningScreenType; }
    public LearningMode getLearningMode() {
        return mLearningMode;
    }

//========================================================================================
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_ITEMS_AMOUNT = "items_amount";

        private View mCardFrontLayout;
        private View mCardBackLayout;
        private View mRadicalBackLayout;
        private View mKanjiMainReadingFrame;
        private ConstraintLayout mKanjiMainReadingLayout;
        private ConstraintLayout mKanjiInfoReadingLayout;
        private View mVocabMainReadingFrame;
        private ConstraintLayout mVocabMainReadingLayout;
        private ConstraintLayout mVocabInfoReadingLayout;
        private View mKanjiBackLayout;
        private View mVocabBackLayout;
        private ImageView shameIcon;
        private BasicData currentItem;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber, int amountOfItems, Kanji kanji) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_ITEMS_AMOUNT, amountOfItems);
            args.putSerializable("kanji", kanji);
            fragment.setArguments(args);
            return fragment;
        }

        public static PlaceholderFragment newInstance(int sectionNumber, int amountOfItems, Vocab vocab) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_ITEMS_AMOUNT, amountOfItems);
            args.putSerializable("vocab", vocab);
            fragment.setArguments(args);
            return fragment;
        }

        public static PlaceholderFragment newInstance(int sectionNumber, int amountOfItems, Radical radical) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_ITEMS_AMOUNT, amountOfItems);
            args.putSerializable("radical", radical);
            fragment.setArguments(args);
            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_learning, container, false);

            mCardBackLayout = rootView.findViewById(R.id.learning_back);
            mCardFrontLayout = rootView.findViewById(R.id.learning_front);

            mRadicalBackLayout = rootView.findViewById(R.id.learning_info_radical);
            mKanjiBackLayout = rootView.findViewById(R.id.learning_info_kanji);
            mVocabBackLayout = rootView.findViewById(R.id.learning_info_vocab);

            mKanjiMainReadingFrame = rootView.findViewById(R.id.learning_main_kanji);
            mVocabMainReadingFrame = rootView.findViewById(R.id.learning_main_vocab);

            shameIcon = rootView.findViewById(R.id.learning_icon);
            shameIcon.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    SharedPreferences shamelist = null;
                    String idString = null;

                    if (currentItem instanceof Radical) {
                        idString = ((Radical) currentItem).getIdString();
                        shamelist = getContext().getSharedPreferences(Radical.SHAME_LIST, MODE_PRIVATE);
                    } else if (currentItem instanceof Kanji) {
                        idString = ((Kanji) currentItem).getCharacter();
                        shamelist = getContext().getSharedPreferences(Kanji.SHAME_LIST, MODE_PRIVATE);
                    } else if (currentItem instanceof Vocab) {
                        idString = ((Vocab) currentItem).getCharacter();
                        shamelist = getContext().getSharedPreferences(Vocab.SHAME_LIST, MODE_PRIVATE);
                    }

                    boolean updatedStatus = currentItem.updateShamelistItem(shamelist, shamelist.edit(), idString, getContext());
                    if (true == updatedStatus) {
                        shameIcon.setImageResource(R.drawable.emoji_red);
                    } else {
                        shameIcon.setImageResource(R.drawable.emoji_gray);
                    }
                }
            });

            ConstraintLayout clMain = rootView.findViewById(R.id.constraintLayoutMain);
            clMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LearningActivity)getActivity()).setLearningScreenType(LearningScreenType.INFO);
                    mCardFrontLayout.setVisibility(View.GONE);
                    mCardBackLayout.setVisibility(View.VISIBLE);
                }
            });

            ConstraintLayout clInfo = rootView.findViewById(R.id.constraintLayoutInfo);
            clInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LearningActivity)getActivity()).setLearningScreenType(LearningScreenType.CHARACTER);
                    mCardFrontLayout.setVisibility(View.VISIBLE);
                    mCardBackLayout.setVisibility(View.GONE);
                }
            });

            mKanjiMainReadingLayout = clMain.findViewById(R.id.kanji_main_reading);
            mKanjiInfoReadingLayout = clInfo.findViewById(R.id.kanji_info_reading);
            mVocabMainReadingLayout = clMain.findViewById(R.id.vocab_main_reading);
            mVocabInfoReadingLayout = clInfo.findViewById(R.id.vocab_info_reading);

            TextView textView = rootView.findViewById(R.id.learning_item_counter);
            textView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)) + "/" +
                    Integer.toString(getArguments().getInt(ARG_ITEMS_AMOUNT)));

            if ( LearningScreenType.CHARACTER == ((LearningActivity)getActivity()).getLearningScreenType()) {
                mCardFrontLayout.setVisibility(View.VISIBLE);
                mCardBackLayout.setVisibility(View.GONE);
            } else {
                mCardFrontLayout.setVisibility(View.GONE);
                mCardBackLayout.setVisibility(View.VISIBLE);
            }

            if (LearningMode.KANJI == ((LearningActivity) getActivity()).getLearningMode()) {

                mRadicalBackLayout.setVisibility(View.GONE);
                mKanjiBackLayout.setVisibility(View.VISIBLE);
                mVocabBackLayout.setVisibility(View.GONE);
                mVocabMainReadingFrame.setVisibility(View.GONE);

                if (0 == ((LearningActivity) getActivity()).getReadingWith()) {
                    bindKanji(rootView, mKanjiMainReadingLayout);
                } else {
                    bindKanji(rootView, mKanjiInfoReadingLayout);
                }

            } else if (LearningMode.VOCAB == ((LearningActivity) getActivity()).getLearningMode()) {
                mRadicalBackLayout.setVisibility(View.GONE);
                mKanjiBackLayout.setVisibility(View.GONE);
                mVocabBackLayout.setVisibility(View.VISIBLE);
                mKanjiMainReadingFrame.setVisibility(View.GONE);

                if (0 == ((LearningActivity) getActivity()).getReadingWith()) {
                    bindVocab(rootView, mVocabMainReadingLayout);
                } else {
                    bindVocab(rootView, mVocabInfoReadingLayout);
                }

            } else if (LearningMode.RADICALS == ((LearningActivity) getActivity()).getLearningMode()) {
                bindRadical(rootView);
                mRadicalBackLayout.setVisibility(View.VISIBLE);
                mKanjiBackLayout.setVisibility(View.GONE);
                mVocabBackLayout.setVisibility(View.GONE);

                mKanjiMainReadingFrame.setVisibility(View.GONE);
                mVocabMainReadingFrame.setVisibility(View.GONE);
            }
            return rootView;
        }

        private void updateShameIcon(BasicData data) {
            if (data.isItemOnShamelist(getContext())) {
                shameIcon.setImageResource(R.drawable.emoji_red);
            } else {
                shameIcon.setImageResource(R.drawable.emoji_gray);
            }
        }

        private void bindKanji(View v, ConstraintLayout c) {
            Kanji kanji = ((Kanji) getArguments().getSerializable("kanji"));
            currentItem = kanji;

            TextView characterText = v.findViewById(R.id.learning_item_chars);
            TextView kunyomiText = c.findViewById(R.id.learning_info_kanji_kunyomi);
            TextView onyomiText = c.findViewById(R.id.learning_info_kanji_onyomi);
            TextView nanoriText = c.findViewById(R.id.learning_info_kanji_nanori);
            TextView meaningText = v.findViewById(R.id.learning_info_kanji_meaning);

            TextView lvl = v.findViewById(R.id.learning_item_lvl_data);
            lvl.setText(Integer.toString(kanji.getLevel()));

            updateShameIcon(kanji);
            characterText.setText(kanji.getCharacter());

            String nanori = kanji.getNanori();
            if (null != nanori && false == nanori.equalsIgnoreCase("None") && false == nanori.isEmpty()) {
                nanoriText.setVisibility(View.VISIBLE);
                nanoriText.setText(getContext().getString(R.string.nanori) + nanori);
            } else {
                nanoriText.setVisibility(View.GONE);
            }

            String onyomi = kanji.getOnyomi();
            if (null != onyomi && false == onyomi.equalsIgnoreCase("None") && false == onyomi.isEmpty()) {
                onyomiText.setVisibility(View.VISIBLE);
                onyomiText.setText(getContext().getString(R.string.onyomi) + onyomi);
            } else {
                onyomiText.setVisibility(View.GONE);
            }

            String kunyomi = kanji.getKunyomi();
            if (null != kunyomi && false == kunyomi.equalsIgnoreCase("None") && false == kunyomi.isEmpty()) {

                kunyomiText.setVisibility(View.VISIBLE);
                kunyomiText.setText(getContext().getString(R.string.kunyomi) + kunyomi);
            } else {
                kunyomiText.setVisibility(View.GONE);
            }

            String meaning = kanji.getMeaning();
            if (null != kanji.getUserSpecific() &&
                    null != kanji.getUserSpecific().getUserSynonyms()) {
                meaning += ", " + TextUtils.join(", ", kanji.getUserSpecific().getUserSynonyms());
            }
            meaningText.setText(meaning);
        }

        private void bindVocab(View v, ConstraintLayout c) {
            Vocab vocab = ((Vocab) getArguments().getSerializable("vocab"));
            currentItem = vocab;

            TextView lvl = v.findViewById(R.id.learning_item_lvl_data);
            lvl.setText(Integer.toString(vocab.getLevel()));

            updateShameIcon(vocab);

            TextView characterText = v.findViewById(R.id.learning_item_chars);
            characterText.setText(vocab.getCharacter());

            TextView readingText = c.findViewById(R.id.learning_info_vocab_reading);
            readingText.setText(vocab.getKana());

            TextView meaningText = v.findViewById(R.id.learning_info_vocab_meaning);
            meaningText.setText(vocab.getMeaning());
        }

        private void bindRadical(View v) {
            Radical radical = ((Radical) getArguments().getSerializable("radical"));
            currentItem = radical;

            updateShameIcon(radical);

            TextView lvl = v.findViewById(R.id.learning_item_lvl_data);
            lvl.setText(Integer.toString(radical.getLevel()));

            TextView charMeaningText = v.findViewById(R.id.learning_info_radical_meaning);
            charMeaningText.setText(radical.getMeaning());

            ImageView characterImage = v.findViewById(R.id.learning_img);
            characterImage.setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY);

            TextView characterText = v.findViewById(R.id.learning_item_chars);
            String character = radical.getCharacter();
            if (null != character && false == character.equalsIgnoreCase("None") && false == character.isEmpty()) {
                characterText.setVisibility(View.VISIBLE);
                characterText.setText(character);
                characterImage.setVisibility(View.GONE);
            } else {
                characterText.setVisibility(View.GONE);
                bindCharacterImg(characterImage, radical);
            }
        }

        private void bindCharacterImg(ImageView imageView, Radical radical) {
            String characterImageUrl = radical.getImage();
            if (null != characterImageUrl && false == characterImageUrl.equalsIgnoreCase("None") && false == characterImageUrl.isEmpty()) {
                imageView.setVisibility(View.VISIBLE);

                Picasso.get().load(characterImageUrl)
                        .error(R.drawable.cross_icon)
                        .into(imageView);
            }
        }
    }
//====================================================================================================
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            PlaceholderFragment p = new PlaceholderFragment();
            if (LearningMode.KANJI == mLearningMode) {
                p = PlaceholderFragment.newInstance(position + 1, getCount(), mKanjiList.get(position));
                currentItem = mKanjiList.get(position);
            } else if (LearningMode.RADICALS == mLearningMode) {
                p = PlaceholderFragment.newInstance(position + 1, getCount(), mRadicalList.get(position));
                currentItem = mRadicalList.get(position);
            } else if (LearningMode.VOCAB == mLearningMode) {
                p = PlaceholderFragment.newInstance(position + 1, getCount(), mVocabList.get(position));
                currentItem = mVocabList.get(position);
            }

            return p;
        }

        @Override
        public int getCount() {
            int count = 0;
            if (LearningMode.KANJI == mLearningMode) {
                count = mKanjiList.size();
            } else if (LearningMode.RADICALS == mLearningMode) {
                count = mRadicalList.size();
            } else if (LearningMode.VOCAB == mLearningMode) {
                count = mVocabList.size();
            }
            return count;
        }
    }
}
