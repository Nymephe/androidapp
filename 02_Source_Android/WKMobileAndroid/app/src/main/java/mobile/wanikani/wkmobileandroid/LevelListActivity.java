package mobile.wanikani.wkmobileandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import mobile.wanikani.wkmobileandroid.models.LearningMode;
import mobile.wanikani.wkmobileandroid.models.LevelListItem;

public class LevelListActivity extends AppCompatActivity implements RecyclerItemClickListener.OnRecyclerClickListener {

    public static final String CUSTOM_LVL_LIST = "CustomLvlList";

    private final List<Integer> mLvls = IntStream.range(1, 60 + 1)
            .boxed()
            .collect(Collectors.toList());

    private LevelsListRecyclerViewAdapter mLevelsListRecyclerViewAdapter;
    private LearningMode mSelectedLearningMode;
    private RecyclerView mListRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_list);

        LearningMode result = (LearningMode) getIntent().getSerializableExtra(LearningMode.LEARNING_MODE);
        mSelectedLearningMode = (null != result) ? result : LearningMode.LEVELS; // just in case
        ArrayList<LevelListItem> customListAvailability =
                (ArrayList<LevelListItem>) getIntent().getSerializableExtra(LevelListActivity.CUSTOM_LVL_LIST);

        mListRecyclerView = findViewById(R.id.levels_list);
        mListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mListRecyclerView, this));

        if (null == customListAvailability) {
            List<LevelListItem> lista = new ArrayList<>();
            ArrayList<String> lvlStrings = getLvlsStrings();
            for (Integer idx = 0; idx < lvlStrings.size(); idx++)
            {
                lista.add( new LevelListItem(idx+1, lvlStrings.get(idx)));
            }

            mLevelsListRecyclerViewAdapter = new LevelsListRecyclerViewAdapter(this, lista);
        } else {
            mLevelsListRecyclerViewAdapter = new LevelsListRecyclerViewAdapter(this, customListAvailability);
        }

        mListRecyclerView.setAdapter(mLevelsListRecyclerViewAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        CheckBox lvlCheckbox = view.findViewById(R.id.levels_checkbox);
        lvlCheckbox.setChecked(!lvlCheckbox.isChecked());
        mLevelsListRecyclerViewAdapter.updateStateList(lvlCheckbox.isChecked(), position);
    }

    private ArrayList<String> getLvlsStrings() {
        ArrayList<String> lvlsStrings = new ArrayList<String>(mLvls.size());
        for (Integer i : mLvls) {
            lvlsStrings.add(String.valueOf(i));
        }
        return lvlsStrings;
    }

    public void onShowDifficultyLevelsClicked(View v) {
        Intent intent = new Intent(this, LevelDifficultyListActivity.class);
        startActivity(intent);
        this.overridePendingTransition(0, 0);
        LevelListActivity.this.finish();
    }

    public void onLevelOkClicked(View v) {

        if (LearningMode.LEVELS == mSelectedLearningMode) {
            Intent intent = new Intent(this, LevelsDetailListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, mSelectedLearningMode);
            intent.putExtra("SelectedLevels", mLevelsListRecyclerViewAdapter.getCheckedLevelsList());
            startActivity(intent);
        } else if (LearningMode.SHAMELIST == mSelectedLearningMode){
            Intent intent = new Intent(this, LevelsDetailListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, mSelectedLearningMode);
            intent.putExtra("SelectedLevels", mLevelsListRecyclerViewAdapter.getCheckedLevelsList());
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, DetailListActivity.class);
            intent.putExtra(LearningMode.LEARNING_MODE, mSelectedLearningMode);
            intent.putExtra("SelectedLevels", mLevelsListRecyclerViewAdapter.getCheckedLevelsList());
            startActivity(intent);
        }
        this.overridePendingTransition(0, 0);
    }
}
