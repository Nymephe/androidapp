package mobile.wanikani.wkmobileandroid.models;

import java.io.Serializable;

public class LevelListItem implements Serializable {
    public int levelId;
    public boolean checked;
    public String text;

    public LevelListItem(int levelId, String text) {
        this.levelId = levelId;
        this.checked = false;
        this.text = text;
    }
}
