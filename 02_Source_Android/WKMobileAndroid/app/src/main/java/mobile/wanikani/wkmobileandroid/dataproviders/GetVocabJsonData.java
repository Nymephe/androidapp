package mobile.wanikani.wkmobileandroid.dataproviders;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import mobile.wanikani.wkmobileandroid.models.RequestedVocabList;

public class GetVocabJsonData extends AsyncTask<String, Void, RequestedVocabList> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetJsonData";

    private RequestedVocabList mRequestedVocabList = null;
    private DownloadStatus mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;

    private final GetVocabJsonData.OnDataAvailable mCallBack;

    public interface OnDataAvailable {
        void onDataAvailable(RequestedVocabList data, DownloadStatus status);
    }

    public GetVocabJsonData(GetVocabJsonData.OnDataAvailable callBack) {
        Log.d(TAG, "GetVocabJsonData called");
        mCallBack = callBack;
    }

    protected void onPostExecute(RequestedVocabList vocabList) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(mRequestedVocabList, mDownloadStatus);
        }
        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected RequestedVocabList doInBackground(String... params) {
        Log.d(TAG, "doInBackground starts");
        String destinationUri =  createUri(params[0], ((params.length > 1)? params[1] : ""));

        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground ends");
        return mRequestedVocabList;
    }

    private String createUri(String userAPIKey, String request) {
        Log.d(TAG, "createUri starts");
        return "https://www.wanikani.com/api/user/" + userAPIKey + "/" + request;
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        mDownloadStatus = status;
        if(status == DownloadStatus.OK) {
            mRequestedVocabList = new RequestedVocabList();

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                mRequestedVocabList = objectMapper.readValue(data, RequestedVocabList.class);

            } catch (IOException e) {
                Log.e(TAG, "onDownloadComplete: Error in parsing Json with mapper" + e.getMessage() );
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
        }
        Log.d(TAG, "onDownloadComplete ends");
    }
}
