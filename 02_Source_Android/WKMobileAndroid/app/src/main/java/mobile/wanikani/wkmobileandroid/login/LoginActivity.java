package mobile.wanikani.wkmobileandroid.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Toast;

import mobile.wanikani.wkmobileandroid.MainActivity;
import mobile.wanikani.wkmobileandroid.R;
import mobile.wanikani.wkmobileandroid.dataproviders.DownloadStatus;
import mobile.wanikani.wkmobileandroid.dataproviders.GetJsonData;
import mobile.wanikani.wkmobileandroid.models.RequestedStudyQueue;

public class LoginActivity extends AppCompatActivity implements GetJsonData.OnDataAvailable {

    private SharedPreferences mSettings;
    private TextInputEditText mApiKeyInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSettings = getSharedPreferences("AppSettings", MODE_PRIVATE);

        if (true == isUserLogged()) {
            // user is already logged so skip that
            skipToMain();
        }

        mApiKeyInput = findViewById(R.id.login_api_key_input);
        mApiKeyInput.setFilters(new InputFilter[] { new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if(source.equals("")){ // for backspace
                    return source;
                }
                if(source.toString().matches("[a-zA-Z0-9]+")){
                    return source;
                }
                return "";
        }
        }});
    }

    @Override
    public void onDataAvailable(RequestedStudyQueue data, DownloadStatus status) {
        if (DownloadStatus.OK == status) {
            saveAPIKey();
            showSuccessToast();
            skipToMain();
        } else {
            showFailureToast();
        }
    }

    public void onButtonClicked(View v) {
        GetJsonData jsonData = new GetJsonData(this);
        jsonData.execute(mApiKeyInput.getText().toString());
    }

    public void showFailureToast() {
        Toast toast = Toast.makeText(getApplicationContext(),
                "Your API key seems to be invalid, or connection error occured.",
                Toast.LENGTH_SHORT);

        toast.show();
    }
    public void showSuccessToast() {
        Toast toast = Toast.makeText(getApplicationContext(),
                "Connection succeed.",
                Toast.LENGTH_SHORT);

        toast.show();
    }
    public void saveAPIKey() {
        SharedPreferences.Editor editor = mSettings.edit();

        editor.putBoolean(getString(R.string.settings_night_mode), false);
        editor.putInt(getString(R.string.settings_reading_with), 0);
        editor.putInt(getString(R.string.settings_show_first), 0);
        editor.putString(getString(R.string.login_api_key), mApiKeyInput.getText().toString());

        editor.apply();
    }

    public void skipToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    public boolean isUserLogged() {
        return mSettings.contains(getString(R.string.login_api_key));
    }
}
